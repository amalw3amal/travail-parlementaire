<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGovRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gov_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->string('request_type')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('ministry_id')->nullable();
            $table->unsignedInteger('district_id')->nullable();
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('file')->nullable();
            $table->string('file_name')->nullable();
            $table->dateTime('created_on')->nullable();
            $table->dateTime('sent_on')->nullable();
            $table->dateTime('response_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gov_requests');
    }
}
