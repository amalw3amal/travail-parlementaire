<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'requests',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('key')->unique();
                $table->unsignedInteger('applicant_id');
                $table->unsignedInteger('category_id');
                $table->unsignedInteger('district_id');
                $table->unsignedInteger('user_type_id')->nullable();
                $table->unsignedInteger('user_id')->nullable();
                $table->unsignedInteger('status_id')->nullable();
                $table->text('proof_file')->nullable();
                $table->text('title');
                $table->text('description');
                $table->boolean('request_information')->default(1);
                $table->boolean('closed')->default(0);
                $table->timestamps();
            }
        );
    }

    /**"
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
