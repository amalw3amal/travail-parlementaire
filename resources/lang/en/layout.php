<?php

return [
    'intro' => 'Welcome to the citizen space of Amal wa 3amal',
    'make_application' => 'Make an application',
    'make_app_description' => 'Please select your electoral district to direct you to your MP.',
    'follow_request' => 'Follow my request',
    'roles' => 'By pressing accept,you agree that, in accordance with article 145 of the rules of procedure of the tunisian parliament, the data concerning a request, after study and filtering made by the deputy, can be transferred to a member of the government in the form of a written question. The questions and the answers will be made public and published.',
    'accepte' => 'Accept',
    'refuse' => 'Refuse',
    'intro_description_1' => '<br>The Amal wa aamal movement makes this website available to you in order to facilitate communication with your representatives at the Assembly of People\'s Representatives and and to monitor the progress of the treatment of your problems.<br>',
    'intro_description' => '<div style="color: #454545;">
    Nous sommes heureux de vous souhaiter la bienvenue sur notre  espace Citoyen, destiné à faciliter vos démarches liées à vos demandes et plaintes .
    <br>
    This space is accessible 24 hours a day, 7 days a week from a computer, smartphone or tablet:
                    <br>
                    You will be able to use the following features:
                    <ul>
                        <li>
                            <i class="fa fa-check" aria-hidden="true" style="color: #2ecc71"></i>
                            File an application or complaint to the Member representing your constituency
                        </li>
                        <li>
                            <i class="fa fa-check" aria-hidden="true" style="color: #2ecc71"></i>
                            View the history of your requests
                        </li>
                        <li>
                            <i class="fa fa-check" aria-hidden="true" style="color: #2ecc71"></i>
                            Track the processing status of your requests
                        </li>
                        <li>
                            <i class="fa fa-check" aria-hidden="true" style="color: #2ecc71"></i>
                            Have secure storage for your vouchers
                        </li>
                    </ul>
                    Our services are at your disposal for any information.
                    </p>
                </div>',

    'id' => 'ID',
    'name' => 'Name',
    'sex' => 'Gender',
    'select_sex' => 'Select your gender',
    'male' => 'Male',
    'female' => 'Female',
    'phone_number' => 'Phone number',
    'email' => 'Email',
    'category' => 'Sector',
    'title' => 'Title',
    'title_placeholder' => '',
    'description' => 'Description',
    'description_placeholder' => '',
    'password' => 'Ticket Code',

    'circonscription' => 'District',
    'send' => 'Send',
    'district' => 'Electoral district',
    'join_files' => 'Attachments',
    'select_category' => 'Select your sector',

 'page_404' => 'Oups !',
    'not_found' => 'The page does not exist or is not available.',
    'go_to_home' => 'Return to the home page',

    'follow' => 'Follow',

    'start' => 'start',
    'tell_us_about_you' => 'Tell us about you',
    'tell_us_about_request' => 'Tell us about your request',


    'proof_of_contact' => 'Proof of contact',
    'our_deputies_at' => 'Our deputies at',

    'about' => 'About',
    'loading' => 'Loading...',
    'select_your' => '-- Make a choice --',
    'create_request' => 'Make a request',
    'transfert_files' => 'Transfert files',

    'p1' => 'We don\'t have deputies in yout electoral district',
    'p2' => 'Please feel free to contact any deputy from your electoral district to let him help you',

    'sms_message' => 'Your request has been registered, to follow your request national ID number: :cin Secret Code: :key',
    'registered_message' => 'Your request has been registered, to follow your request <br><b>national ID number</b>: :cin <br><b>Secret Code</b>: :key',
	'exist_message' => 'You have already request registered, you can follow it directly on the page <Follow Your request> <br><b>national ID number</b>: :cin <br><b>Secret Code</b>: :key',

    'email_subscription' => 'subscription',
    'email_contact' => 'to contact us',
    'email_title' => 'Saved request',
    'email_message' => '
            Hello :user_name
    <p>Your request has been registered </p>
    <p>We are currently studying this request and you can follow it directly via  <a style="text-decoration: underline;font-weight: bold;" href="https://question.mouvementamal.tn/en/request?cin=:cin&password=:key" target="_blank">this link</a></p>

    <p>or by <a style="text-decoration: underline;font-weight: bold;" href="https://question.mouvementamal.tn/en/request" target="_blank">the application</a> entering the following information :</p>
    <p>  national ID number :cin secret code :key</p>
    <br /> <br />
    Best regards.
    ',
    'select_district' => 'Select your district',
    'gov_requested' => 'Gov Requested',
     'text_gov_requested' => 'This website is accessible 24 hours a day throughout the week, from your computer, smartphone or tablet.
You can use the following features :
 Submit a proposal, request, or complaint to the representative corresponding to your constituency
Monitor the progress of the traitment of your requests.',
    'email_subject' => 'Request registred',

    'confidentialityTitle' => 'TERMS OF USE',

     'copyright' => '© Copyright 2020 MouvementAmal',
    'confidentiality' => '<div style="color: #454545;">
    <p>
By having access to the Site question.mouvementamal.tn, you confirm your understanding of the General terms of Use and agree to respect them, if you do not accept these General terms of Use, we ask you not to use the Site.<br>
The Site reserves the right at any time to change, modify, add or delete, totally or partially, the provisions of these General terms of Use.<br>
<br>
I.	LEGAL USE:<br>
-	When you create a request on the Site, you will be required to provide us certain number of  information.<br>
-	You must provide true, precise, current and complete information concerning you, required by Mouvementamal in the creation form.<br>
-	You are responsible for maintaining confidentiality, restricting access to your requests and agree to assume responsibility for all activities performed.<br>
-	You cannot, at any time, use the access of a third-party, without the express agreement of its holder.<br>
-	You agree that we may communicate with you through electronic messaging (e-mail), SMS and telephone.<br>
-	You are responsible for ensuring that your use of the Site complies with these Terms of Use and according to applicable law and generally accepted practices.<br>
<br>
II.	VIOLATION OF THESE TERMS OF USE:<br>
If it appears that your use of the Site violates these Terms of Use, or when Mouvementamal has reasonable grounds to believe that this violation has occurred, Mouvementamal reserves the right to temporarily close or block access to the Site immediately and without prior warning.<br>
<br>
III.	ACCESSIBILITY:<br>
Your access to the site may, from time to time, be totally or partially inaccessible due to system maintenance or for other reasons. As soon as possible, we will provide you with information about any limitations on accessibility.<br>
<br>
How to contact us?
If you have any questions or comments about these Terms of Use, please contact us at the following address: contact@mouvementamal.tn<br>
<br>
IV.	PRIVACY POLICY:<br>
As a user of the application, you benefit, in accordance with Organic Law n ° 2004-63 dated July 27, 2004 relating to the protection of personal data, of a right of access, opposition and rectification of personal data which concerns you and which you may communicate within the framework of the use of the site.<br>
The Data that you communicate to us when creating a request, or when updating it, is collected and processed by Mouvement Amal in accordance with the regulations in force.<br>
<br>
V.	PERSONAL DATA:<br>
1-	The data you give us:<br>
-	Number of the national identity card, it is the unique identifier to follow the requests.<br>
-	Name and first name: are used to identify applicants.<br>
-	E-mail address: E-mail to receive notifications related to your requests.<br>
-	Phone number: to receive SMS containing the access code for tracking requests.<br>
-	Supporting documents: used to obtain details of requests as well as supporting documents, which can be transferred to the ministers concerned.<br>
These data are used by Mouvementamal in order to study the request and process it near the departments concerned. All this data will be destroyed after 3 months from the closing of the request.<br>
2-	The data collected when you use the site:<br>
-	Information about your device, connection to our services and use of multiple devices. This information may, among other things, include the operating system, version of the web browser, IP addresses.<br>
-	Information on how you use the site. We save when you log in or log out of your account.<br>
This data is used by Mouvementamal in order to secure the site and improve the service.<br>
<br>
Why do we process and share your information?<br>
In accordance with article 145 of the rules of procedure of the assembly of the representatives of the people, The data concerning a request, after study and filtering made by the deputy, can be transferred to a member of the government in the form of a written question, Questions and answers will be published.<br>
Finally, Mouvementamal, has declared the processing of personal data in accordance with organic law n ° 2004-63 dated July 27, 2004 relating to the protection of personal data. To this end, Mouvementamal is committed to protecting the confidentiality of your Data.<br>
<br>
In order to exercise your rights of access, rectification and opposition, you can send your request to: contact@mouvementamal.tn
</p>
</div>',
     'Written_question' => 'Question écrite',
    'Oral_question' => 'Question orale',
    'Direct_messaging' => 'Correspondance directe',
    'Answer_to_a_written_question' => 'Une réponse à une question écrite',

    'notify_user_subject' => 'Votre dossier a été mis à jour',
	'notify_user_body' => '
            Bonjour :user_name<br><br>
    <p>Votre dossier a été mis à jour</p>
    <p>Vous pouvez le suivre ou interagir avec l\'équipe via  <a style="text-decoration: underline;font-weight: bold;" href="https://question.mouvementamal.tn/fr/request?cin=:cin&password=:key" target="_blank">ce lien</a></p>

    <p>ou via <a style="text-decoration: underline;font-weight: bold;" href="https://question.mouvementamal.tn/fr/request" target="_blank">l\'application</a> en entrant les informations suivantes :</p>
    <p>  CIN :cin Code secret :key</p>
    <br /> <br />
    Cordialement.
    ',
	

    'notify_user_title' => 'Votre dossier a été mis à jour',

    'notify_responsible_subject' => 'Vous êtes maintenant responsable d\'un dossier',
    'notify_responsible_body' => 'Bonjour, <br><br>
	Vous êtes maintenant responsable d\'un dossier<br>
	Numéro du dossier  :id <br><br>
	Merci.',
    'notify_responsible_title' => 'Mise à jour dossier sur question.mouvementamal.tn',
	
	 'notify_admin_resp_subject' => 'Un demandeur a mis à jour son dossier',
    'notify_admin_resp_body' => 'Bonjour, <br><br>
	Le demandeur a mis à jour son dossier<br>
	Numéro du dossier  :id <br><br>
	Merci.',
    'notify_admin_resp_title' => 'Mise à jour dossier sur question.mouvementamal.tn',
];
