<?php

return [
    // Nav bar left
    'dashboard' => 'dashboard',
    'main_navigation' => 'main_navigation',
    'requests' => 'requests',
    'applicant' => 'applicant',
    'gov_request' => 'gov_request',
    'users_management' => 'users_management',
    'privileges_roles' => 'privileges_roles',
    'users' => 'users',
    'profiles' => 'profiles',
    'settings' => 'settings',
    'categories' => 'categories',
    'ministry' => 'ministry',
    'teams' => 'teams',
    'status' => 'status',
    'super_admin' => 'super_admin',
    'log_users_access' => 'log_users_access',
    'logs' => 'logs',

    // Requests Tab Rest
    'id' => 'id',
    'key' => 'key',
    'moderator' => 'moderator',
    'district' => 'district',
    'category' => 'category',
    'updated_at' => 'updated_at',
    'action' => 'action',

    // Requets Filter Rest
    'closed' => 'closed',
    'valid' => 'valid',
    'all' => 'all',
    'yes' => 'yes',
    'no' => 'no',
    'has_gov_request' => 'has_gov_request',
    'created_on' => 'created_on',
    'waiting' => 'waiting',
    'filter' => 'filter',
	'title' => 'Title',
];
