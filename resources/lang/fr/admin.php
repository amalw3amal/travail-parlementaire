<?php

return [
    // Nav bar left
    'dashboard' => 'Tableau de bord',
    'main_navigation' => 'Menu général',
    'requests' => 'Demandes',
    'applicant' => 'Demandeurs',
    'gov_request' => 'Correspondances <br>avec le gouvernement',
    'users_management' => 'Gestion des utilisateurs',
    'privileges_roles' => 'Gestions des privilèges',
    'users' => 'Utilisateurs',
    'profiles' => 'Profils',
    'districts' => 'Régions',
    'settings' => 'Paramètres',
    'categories' => 'Catégories',
    'ministry' => 'Ministères',
    'teams' => 'Equipes',
    'status' => 'Status',
    'super_admin' => 'Super Admin',
    'log_users_access' => 'journal accés utilisateurs',
    'logs' => 'Fichier journal',

    // Requests Tab Rest
    'id' => 'Identifiant',
    'key' => 'Clé',
    'moderator' => 'Responsable',
    'district' => 'Région',
    'category' => 'Catégorie',
    'updated_at' => 'Mis à jour (par le client)',
	'created_at' => 'Crée',
    'action' => 'Action',

    // Requets Filter Rest
    'closed' => 'Fermé',
    'valid' => 'Validé',
    'all' => 'Tout',
    'yes' => 'Oui',
    'no' => 'Non',
    'has_gov_request' => 'a une correspondance associée',
    'created_on' => 'Crée entre',
	'updated_on' => 'Mis à jour (par le client) le',
    'waiting' => 'En attente',
    'filter' => 'Filtrer',
	'title' => 'Titre',
	'rechercher' => 'Rechercher',
	'conv_status' => 'Statut de la conversation',
];
