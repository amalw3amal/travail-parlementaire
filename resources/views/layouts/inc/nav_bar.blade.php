<nav class="navbar navbar-expand-md navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url(app()->getLocale()) }}">
            <img src="{{ asset('images/logoAmal.png') }}" class="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <i class="fa fa-bars" aria-hidden="true"  style="color: #111111!important; font-size: 30px;"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
            </ul>
            <!-- Right Side Of Navbar -->
            <div class="navbar-nav ml-auto">
                <li class="nav-item follow-request-btn">
                    <a href="{{ route('about', ['locale' => app()->getLocale()]) }}" class="nav-link">@lang('layout.about')</a>
                </li>
                <li class="nav-item follow-request-btn">
                    <a href="{{ route('show_request', ['locale' => app()->getLocale()]) }}" class="nav-link">@lang('layout.follow_request')</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ app()->getLocale() }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url('fr') }}">Francais</a>
                        <a class="dropdown-item" href="{{ url('en') }}">English</a>
                        <a class="dropdown-item" href="{{ url('ar') }}">عربى</a>
                    </div>
                </li>
            </div>
        </div>
    </div>
</nav>
