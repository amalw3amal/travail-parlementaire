<footer id="footer">

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p>
                    <a href="{{ route('confidentiality', [app()->getLocale()]) }}">@lang('layout.confidentialityTitle')</a>
                    |
                    <span>@lang('layout.copyright')</span>
                </p>
            </div>
        </div>
    </div>

</footer>
