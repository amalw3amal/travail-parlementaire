<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li>
                <a href="{{ url('/admin') }}">@lang('admin.dashboard')</a>
            </li>
            <li class="header">@lang('admin.main_navigation')</li>
            <li class="nav-item">
                <a href="{{ route('requests') }}"
                   class="nav-link {{ (\Request::route()->getName()== 'requests')?'active':'' }} ">
                    @lang('admin.requests')
                </a>
            </li>
            @can('applicant')
                <li class="nav-item">
                    <a href="{{ route('applicant') }}"
                       class="nav-link {{ (\Request::route()->getName()== 'applicant')?'active':'' }}">
                        @lang('admin.applicant')
                    </a>
                </li>
            @endcan
            @can('gov_request')
                <li class="nav-item">
                    <a href="{{ route('gov_request') }}"
                       class="nav-link {{ (\Request::route()->getName()== 'gov_request')?'active':'' }}">
                        @lang('admin.gov_request')
                    </a>
                </li>
            @endcan
            @can('user_managemen_roles')
                <li class="header">@lang('admin.users_management')</li>
                @can('roles')
                    <li>
                        <a href="{{ route('user_types') }}"
                           class="nav-link {{ in_array(\Request::route()->getName(), ['user_types' ,'create_user_type','show_user_type'])?'active':'' }}">
                            @lang('admin.privileges_roles')
                        </a>
                    </li>
                @endcan
                @can('user_management')
                    <li>
                        <a href="{{ route('admin_users') }}"
                           class="nav-link has-sub-menu {{ in_array(\Request::route()->getName(), ['admin_users' ,'create_admin_user'])?'active':'' }}">
                            @lang('admin.users')
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('profiles') }}"
                           class="nav-link {{ in_array(\Request::route()->getName(), ['profiles','create_profile'])?'active':'' }}">
                            @lang('admin.profiles')
                        </a>
                    </li>
                @endcan
            @endcan
            @can('settings')
                <li class="header">@lang('admin.settings')</li>
                <li>
                    <a href="{{ route('categories') }}"
                       class="nav-link {{ in_array(\Request::route()->getName(), ['categories','create_category'])?'active':'' }}">
                        @lang('admin.categories')
                    </a>
                </li>
                <li>
                    <a href="{{ route('ministry') }}"
                       class="nav-link {{ in_array(\Request::route()->getName(), ['ministry','create_ministry'])?'active':'' }}">
                        @lang('admin.ministry')
                    </a>
                </li>
                <li>
                    <a href="{{ route('team') }}" class="nav-link {{ in_array(\Request::route()->getName(), ['team','create_team'])?'active':'' }}">
                        @lang('admin.teams')
                    </a>
                </li>
                <li>
                    <a href="{{ route('districts') }}"
                       class="nav-link {{ in_array(\Request::route()->getName(), ['districts','create_district'])?'active':'' }}">
                        @lang('admin.districts')
                    </a>
                </li>
                <li>
                    <a href="{{ route('status') }}"
                       class="nav-link {{ in_array(\Request::route()->getName(), ['status','create_status'])?'active':'' }}">
                        @lang('admin.status')
                    </a>
                </li>
            @endcan
            <li class="header">@lang('admin.super_admin')</li>
            <li>
                <a href="#">
                    @lang('admin.log_users_access')
                </a>
            </li>
            <li>
                <a href="{{ url('logs') }}">
                    @lang('admin.logs')
                </a>
            </li>
        </ul>
    </section>
</aside>
