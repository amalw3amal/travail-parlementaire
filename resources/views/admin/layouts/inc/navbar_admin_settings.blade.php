<div class="col-3">
    <div class="card settings-card">
        <div class="card-body">
            <ul class="nav flex-column nav-pills" aria-orientation="vertical">
                <a href="{{ url('admin/settings/categories') }}" class="nav-link {{ in_array(\Request::route()->getName(), ['categories' ,'create_category'])?'active':'' }}">
                    @lang('admin.categories')
                </a>
                <a href="{{ url('admin/settings/districts') }}" class="nav-link {{ in_array(\Request::route()->getName(), ['districts' ,'create_district'])?'active':'' }}">
                    @lang('admin.districts')
                </a>
                <a href="{{ url('admin/settings/status') }}" class="nav-link {{ in_array(\Request::route()->getName(), ['status' ,'create_status'])?'active':'' }}">
                    @lang('admin.status')
                </a>

            </ul>
        </div>
    </div>
</div>
