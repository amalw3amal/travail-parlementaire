<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    @yield('after_style')
</head>
<body>
<div id="app">
    @include('admin.layouts.inc.nav_bar')
    <main class="py-4">
        <div class="container-fluid">
            <div class="row justify-content-center">
                @include('admin.layouts.inc.nav_bar_left')
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<!-- Scripts -->
@if(\Request::route()->getName() == 'show_admin_request')
    <script src="{{ asset('js/app.js') }}" defer></script>
@else
    <script src="{{ asset('js/app.js') }}"></script>
@endif
@yield('after_script')
</body>
</html>
