@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdn.datatables.net/v/dt/dt-1.10.18/sc-1.5.0/datatables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('section_title')
    Création demande
@endsection

@section('content')

    @include('errors.errors')
    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {!! $flash !!}
        </div>
    @endif
    <div class="row">
        <div class="col-md-8">
            <form action="{{ url('admin/request_save') }}" method="post">
                {{ csrf_field() }}
				
				
				 <div class="form-group row" v-if="loading == false && message.length < 1">
                        <label for="start_district"
                               class="col-md-4 col-form-label text-md-right">@lang('layout.district')</label>
                        <div class="col-md-5">
                            <select class="form-control form-control-lg" id="start_district"
                                    name="district">
                                @foreach($districts as $district)
                                    <option value="{{ $district->id }}">
                                        @if(app()->getLocale() == 'ar')
                                            {{ $district->name_ar }}
                                        @elseif(app()->getLocale() == 'fr')
                                            {{ $district->name_fr }}
                                        @else
                                            {{ $district->name_en }}
                                        @endif
                                    </option>
                                @endforeach
                            </select>
                            
                        </div>
                    </div>
               
					<div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">@lang('layout.category')</label>
                        <div class="col-md-5">
                            <select class="form-control form-control-lg"  required
                                    name="category">
                                <option>**</option>
                                @foreach($categories as $category)
                                    <option
                                        value="{{ $category->id }}" {{ old('category')? old('category')==$category->id?'selected':'':'' }}>
                                        @if(app()->getLocale() == 'fr')
                                            {{ $category->name_fr }}
                                        @elseif(app()->getLocale() == 'ar')
                                            {{ $category->name_ar }}
                                        @else
                                            {{ $category->name_en }}
                                        @endif
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
					
					 <div class="form-group row">
                        <label for="title" class="col-md-4 col-form-label text-md-right">@lang('layout.title')</label>
                        <div class="col-md-5">
                            <input class="form-control form-control-lg" name="title" id="title" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description"
                               class="col-md-4 col-form-label text-md-right">@lang('layout.description')</label>
                        <div class="col-md-8">
                            <textarea class="form-control form-control-lg" name="description"
                                      id="description" required></textarea>
                        </div>
                    </div>



                <div class="col-md-8 offset-md-4">
                    <button class="btn btn-primary">Ajouter demande</button>
                </div>
            </form>
        </div>
    </div>

@endsection
