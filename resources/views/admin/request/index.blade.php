@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('section_title')
    Demandes
@endsection

@section('content')

    <!--<div class="card-header">
        @lang('admin.filter')
    </div>-->
    <div class="card-body">
        <form method="get" class="row">
		
		<div class="col-md-12">
		
		<div class="col-md-3">
                <div class="form-group">
                    <label>@lang('admin.teams')</label>
                    <select class="form-control" name="team">
                        <option value="">@lang('admin.all')</option>
                        @foreach($teams as $team)
                            <option value="{{ $team->id }}" {{ isset($reqs['team'])?$reqs['team']==$team->id?'selected':'':'' }}>{{ $team->name_fr }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
			
			
            <div class="col-md-3">
                <div class="form-group">
                    <label>@lang('admin.status')</label>
                    <select class="form-control" name="status">
                        <option value="">@lang('admin.all')</option>
                        
                        @foreach($status as $statu)
                            <option value="{{ $statu->id }}" {{ isset($reqs['status'])?$reqs['status']==$statu->id?'selected':'':'' }}>{{ $statu->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
			
			
			 <div class="col-md-3">
                <div class="form-group">
                    <label>@lang('admin.moderator')</label>
                    <select class="form-control" name="user">
                        <option value="">@lang('admin.all')</option>
                        @foreach($users as $user)
                            <option value="{{ $user->id }}" {{ isset($reqs['user'])?$reqs['user']==$user->id?'selected':'':'' }}>{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
			
			
             <div class="col-md-3">
                <div class="form-group">
                    <label>@lang('admin.applicant')</label>
                    <select class="form-control" name="applicant">
                        <option value="">@lang('admin.all')</option>
                        @foreach($applicants as $applicant)
                            <option value="{{ $applicant->id }}" {{ isset($reqs['applicant'])?$reqs['applicant']==$applicant->id?'selected':'':'' }}>{{ $applicant->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
			
			
			
			
			
			
			
           
			
		</div>
			
		<div class="col-md-12">
		
		
		    <div class="col-md-3">
                <div class="form-group">
                    <label>@lang('admin.district')</label>
                    <select class="form-control" name="district">
                        <option value="">@lang('admin.all')</option>
                        @foreach($districts as $disticts)
                            <option value="{{ $disticts->id }}" {{ isset($reqs['district'])?$reqs['district']==$disticts->id?'selected':'':'' }}>{{ $disticts->name_en }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
			
           
            
            <div class="col-md-3">
                <div class="form-group">
                    <label>@lang('admin.has_gov_request')</label>
                    <select class="form-control" name="hasGovReq">
                        <option value="" {{ isset($reqs['hasGovReq'])?$reqs['hasGovReq']==''?'selected':'':'' }} >@lang('admin.all')</option>
                        <option value="yes" {{ isset($reqs['hasGovReq'])?$reqs['hasGovReq']=='yes'?'selected':'':'' }} >@lang('admin.yes')</option>
                        <option value="no" {{ isset($reqs['hasGovReq'])?$reqs['hasGovReq']=='no'?'selected':'':'' }} >@lang('admin.no')</option>
                    </select>
                </div>
            </div>
			
			<div class="col-md-3">
                <div class="form-group">
                    <label>@lang('admin.created_on')</label>
                    <date-select></date-select>
                </div>
            </div>
			
			
			<div class="col-md-3">
                <div class="form-group">
                    <label>@lang('admin.updated_on')</label>
					<div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control datepicker pull-right" name="updated" value=" {{ isset($reqs['updated'])? $reqs['updated']:'' }}">
                    </div>
                </div>
            </div>
			
		</div>
		
		<div class="col-md-12">
		    <div class="col-md-3">
                <div class="form-group">
                    <label>@lang('admin.conv_status')</label>
                    <select class="form-control" name="conv_status">
                        <option value="">@lang('admin.all')</option>
                        @foreach($conv_status as $conv_statu)
                            <option value="{{ $conv_statu->id }}" {{ isset($reqs['conv_status'])?$reqs['conv_status']==$conv_statu->id?'selected':'':'' }}>{{ $conv_statu->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
			
		</div>
			
			
            <div class="form-group col-md-12">
			<div class="col-md-12">
                <button class="btn btn-primary" type="submit">@lang('admin.rechercher')</button>
				</div>
            </div>
        </form>
    </div>

    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif
    <table id="table" class="table table-striped table-hover table-bordered" style="width:100%">
        <thead class="thead-dark">
        <tr>
            <th>@lang('admin.id')</th>
            <th>@lang('admin.key')</th>
            <th>@lang('admin.title')</th>
            <th>@lang('admin.applicant')</th>
            <th>@lang('admin.moderator')</th>
            <th>@lang('admin.district')</th>
            <th>@lang('admin.category')</th>
            <th>@lang('admin.status')</th>
            <!--<th>@lang('admin.closed')</th>-->
			<th style="width: 150px;">@lang('admin.created_at')</th>
            <th style="width: 150px;">@lang('admin.updated_at')</th>
            <th>@lang('admin.action')</th>
        </tr>
        </thead>
    </table>
	
	 <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 text-right">
            <a href="{{ route('request_create') }}" class="btn btn-primary">Ajouter une demande interne</a>
        </div>
    </div>
@endsection

@section('after_script')

    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
				responsive: true,
                order: [0,'desc'],
				aaSorting: [],
                processing: true,
                serverSide: true,
				language: {
				url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/French.json"
				},
                ajax: {
                    url: "{{ url('/api/admin/requests_list') }}",
                    data: {
                        status: '{{ isset($reqs['status'])?$reqs['status']:null }}',
                        district: '{{ isset($reqs['district'])?$reqs['district']:null }}',
                        user: '{{ isset($reqs['user'])?$reqs['user']:null }}',
                        closed: '{{ isset($reqs['closed'])?$reqs['closed']:null }}',
                        applicant: '{{ isset($reqs['applicant'])?$reqs['applicant']:null }}',
                        date: '{{ isset($reqs['date'])?$reqs['date']:null }}',
                        hasGovReq: '{{ isset($reqs['hasGovReq'])?$reqs['hasGovReq']:null }}',
						team: '{{ isset($reqs['team'])?$reqs['team']:null }}',
						updated: '{{ isset($reqs['updated'])?$reqs['updated']:null }}',
						conv_status: '{{ isset($reqs['conv_status'])?$reqs['conv_status']:null }}',
                    }
                },
                columns: [
                    { "data": "id" },
                    { "data": "key" },
                    {
                        "data": "title",
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "applicant",
                        "render": function(data){
                            return data.name;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "user",
                        "render": function(data){
                            return data?data.name:'--';
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "district",
                        "render": function(data){
                            return data.name_fr;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "category",
                        "render": function(data){
                            return data.name_fr;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "status",
                        "render": function(data){
                            if(data != null){
                                return '<i class="fa fa-circle" style="font-size:20px; color:#' + data.color + ';" title="' + data.name + '" ></i>'
                            }else{
                                return '<i class="fa fa-circle" style="font-size:20px; color:#757575;" title="' + data.name + '" ></i> Waiting'
                            }
                            return data;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    /*{
                        "data": "closed",
                        "render": function(data){
                            if(data==true){
                                return true
                            }
                            return false;
                        },
                        "searchable": false,
                        "orderable":false
                    },*/
					 {
                        "data": "created_at",
                        "searchable": false,
                        "orderable":true
                    },
                    {
                        "data": "mis_a_jour",
                        "searchable": false,
                        "orderable":true,
						"name": "mis_a_jour"
                    },
                    {
                        "data": "action",
                        "searchable": false,
                        "orderable":false
                    }
                ]
            });
        });
    </script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
    <script>
        $(function () {
            //Date picker
            $('.datepicker').datepicker({
                autoclose: true,
                dateFormat: 'yyyy-mm-dd',
                format: 'yyyy-mm-dd'
            });
        });
    </script>
@endsection
