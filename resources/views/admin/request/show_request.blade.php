@extends('admin.layouts.app')

@section('after_style')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('section_title')
    DEMANDE: {{ $request->id }} | Titre: {{ $request->title }}
@endsection

@section('content')

    <div class="row">
        @if($flash = session('message'))
            <div class="col-md-12">
                <div class="alert alert-success">
                    <i class="fa fa-bell" aria-hidden="true"></i>
                    {{ $flash }}
                </div>
            </div>
        @endif
        <div class="col-md-12">
            @include('errors.errors')
        </div>
       
	   
	   
        <div class="col-md-6">
            <div class="card shadow">
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            Créé par {{ $request->applicant->gender === "Male" ? "Monsieur" : "Madame" }} <a href="{{ route('show_applicant', [$request->applicant->id]) }}">{{ $request->applicant->name }}</a>, le {{ $request->created_at }}<br>
                            <u>CIN</u>: {{ $request->applicant->cin }}<br>
							<u>Téléphone</u>: {{ $request->applicant->phone_number}}<br>
							<u>Email</u>: {{ $request->applicant->email}}
                        </li>
						<li class="list-group-item">Catégorie: {{ $request->category->name_fr }}</li>
                        <li class="list-group-item"> <u>Statut de la demande:</u>
                            @if($request->status)
                                @if($request->status->parent_id != null)
                                    <span style="color: {{ '#'.$request->status->parent->color }}"> <b>{{ $request->status->parent->name }}</b> <i class="fa fa-circle"></i></span>
                                @else
                                    <span style="color: {{ '#'.$request->status->color }}"> <b>{{ $request->status->name }}</b> <i class="fa fa-circle"></i></span>
                                @endif
                            @else
                                <span style="color: #757575"><b>En attente de validation</b> <i class="fa fa-circle"></i></span>
                            @endif
                           <!-- @if($request->updated_at != $request->created_at)
                                <br>Mise a jour le {{ $request->updated_at }}
                            @endif -->
                        </li>
						
						
						
						<li class="list-group-item"><u>Statut de la conversation:</u>
                            @if($request->conv_status)
                               
                                    <span> <b>{{ $request->conv_status->name }}</b> 
									@if($request->conv_status->id==1)
									<i class="fa fa-envelope-o"></i>
								    @else
									<i class="fa fa-envelope-open-o"></i>
								    @endif
									</span>

                            @endif
                        </li>
						
						
						
						
						
                        @if($request->user)
                            <li class="list-group-item">Responsable: {{ $request->user->name }}</li>
                        @endif
                    </ul>
                </div>
            </div>
      
            @if($update)
                <div class="panel panel-default">
                    <div class="panel-heading collapsed"
                         data-toggle="collapse"
                         href="#collapseSettingsApplicant"
                         role="button"
                         aria-expanded="true"
                         aria-controls="collapseSettingsApplicant">
                        Demandeur
                    </div>
                    <div class="collapse in show" id="collapseSettingsApplicant">
                        <div class="panel-body">
                            <form method="post" action="{{ route('update_applicant') }}">
                                @csrf
                                <input type="hidden" name="request_id" value="{{ $request->id }}">
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        CIN
                                    </label>
                                    <div class="col-md-8">
                                        <input class="form-control" name="cin" value="{{ $request->applicant->cin }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Nom
                                    </label>
                                    <div class="col-md-8">
                                        <input class="form-control" name="name" value="{{ $request->applicant->name }}">
                                    </div>
                                </div>
								
								<div class="form-group row">
								    <label class="col-md-4 col-form-label text-md-right">
                                        Sexe 
                                    </label>
                                    <div class="col-md-8">
										<select name="gender" id="gender" class="form-control form-control-lg">
										<option value="Male" {{ $request->applicant->gender === "Male" ? "Selected" : "" }}>Male</option> 
										<option value="Female" {{ $request->applicant->gender === "Female" ? "Selected" : "" }}>Female</option>
										</select>
									</div>
								</div>
								 <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Téléphone
                                    </label>
                                    <div class="col-md-8">
                                        <input class="form-control" name="phone_number" value="{{ $request->applicant->phone_number }}">
                                    </div>
                                </div>
								<div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Email
                                    </label>
                                    <div class="col-md-8">
                                        <input class="form-control" name="email" value="{{ $request->applicant->email }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-8 offset-md-4">
                                        <button class="btn btn-dark">
                                            Mettre à jour <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading"
                         data-toggle="collapse"
                         href="#collapseSettings"
                         role="button"
                         aria-expanded="true"
                         aria-controls="collapseSettings">
                        Réglages
                    </div>
                    <div class="collapse  in show" id="collapseSettings">
                        <div class="panel-body">
                            <form method="post" action="{{ route('update_request') }}">
                                @csrf
                                <input type="hidden" name="request_id" value="{{ $request->id }}">
								 <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Catégorie
                                    </label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="category_id">
                                            <option value="">--</option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}" {{ ($request->category_id == $category->id)?'selected':'' }}>{{ $category->name_fr }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
								
								
								 <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Titre
                                    </label>
                                    <div class="col-md-8">
                                       <input class="form-control" name="title" value="{{ $request->title }}">
                                    </div>
                                </div>
								
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Région
                                    </label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="district_id">
                                            <option value="">--</option>
                                            @foreach($districts as $district)
                                                <option value="{{ $district->id }}" {{ ($request->district_id == $district->id)?'selected':'' }}>{{ $district->name_fr }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Equipe
                                    </label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="team">
                                            <option value="">--</option>
                                            @foreach($teams as $team)
                                                <option value="{{ $team->id }}" {{ ($request->team_id == $team->id)?'selected':'' }}>{{ $team->name_fr }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Responsable
                                    </label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="user">
                                            <option value="">--</option>
                                            @foreach($users as $user)
                                                <option value="{{ $user->id }}" {{ ($request->user_id == $user->id)?'selected':'' }}>{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Statut demande
                                    </label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="status">
                                            @foreach($status as $statu)
                                                <option value="{{ $statu->id }}" {{ ($request->status_id == $statu->id)?'selected':'' }}>{{ $statu->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
								
                                <div class="form-group row">
                                    <div class="col-md-8 offset-md-4">
                                        <button class="btn btn-dark">
                                            Mettre à jour <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
				
				
				<div class="panel panel-default">
                    <div class="panel-heading"
                         data-toggle="collapse"
                         href="#collapseSettings_prive"
                         role="button"
                         aria-expanded="true"
                         aria-controls="collapseSettings_prive">
                       Autres reglages Privés
                    </div>
                    <div class="collapse" id="collapseSettings_prive">
                        <div class="panel-body">
                            <form method="post" action="{{ route('update_request_prive') }}">
                                @csrf
                                <input type="hidden" name="request_id" value="{{ $request->id }}">
             
								 <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Statut conversation
                                    </label>
                                    <div class="col-md-8">
                                        <select class="form-control" name="conv_status">
                                            @foreach($conv_status as $conv_statu)
                                                <option value="{{ $conv_statu->id }}" {{ ($request->conv_status_id == $conv_statu->id)?'selected':'' }}>{{ $conv_statu->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">
                                        Le demandeur peut répondre
                                    </label>
                                    <div class="col-md-8">
                                        <input type="checkbox" name="request_information" {{ ($request->request_information==1)?'checked':'' }}>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-8 offset-md-4">
                                        <button class="btn btn-dark">
                                            Enregistrer <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
				
				
				
				
				
				
				
				
				
				
				
				
            @endif
            <div class="panel panel-default">
                <div class="panel-heading"
                     data-toggle="collapse"
                     href="#collapseGovRequest"
                     role="button"
                     aria-expanded="true"
                     aria-controls="collapseGovRequest">
                    Correspondance à l'administration / Gouvernement
                </div>
                <div class="collapse  in show" id="collapseGovRequest">
                    <div class="panel-body">
                        <form method="POST" action="{{ route('store_gov_request') }}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="requests" value="{{ $request->id }}">
                            <div class="form-group row">
                                <label for="request_type" class="col-md-4 col-form-label text-md-right">
                                    Type *
                                </label>
                                <div class="col-md-8" id="request_type">
                                    <select name="request_type" required class="form-control">
                                        <option value="">Selectionner un type</option>
                                <option value="Written_question">سؤال كتابي</option>
                                <option value="Oral_question">سؤال شفاهي</option>
                                <option value="Direct_messaging">مراسلة مباشرة</option>
                                <option value="Answer_to_a_written_question">جواب على سؤال كتابي</option>
                                    </select>
                                </div>
                            </div>
							
							<div class="form-group row">
								<label for="request_unique_number" class="col-md-4 col-form-label text-md-right">
									Numéro de correspondance *
								</label>
								<div class="col-md-8">
									<input id="request_unique_number" type="text" class="form-control" name="request_unique_number" required>
								</div>
							</div>
							
							<div class="form-group row">
								<label for="request_unique_number" class="col-md-4 col-form-label text-md-right">
								   Année *
								</label>
								<div class="col-md-8">
									<select id="ddlYears" class="form-control" name="annee" required>></select>
								</div>
							</div>
							
                            <div class="form-group row">
                                <label for="title" class="col-md-4 col-form-label text-md-right">
                                    Titre *
                                </label>
                                <div class="col-md-8">
                                    <input id="title" type="text" class="form-control" name="title" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">
                                    Description *
                                </label>
                                <div class="col-md-8">
                                    <textarea id="description" name="description" class="form-control" required></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    Région *
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="district" required>
                                        <option value="">--</option>
                                        @foreach($districts as $district)
                                            <option value="{{ $district->id }}">{{ $district->name_fr }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    Catégorie *
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="category" required>
                                        <option value="">--</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name_fr }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    Ministère *
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" required name="ministry" required>
                                        <option value="">--</option>
                                        @foreach($ministries as $ministry)
                                            <option value="{{ $ministry->id }}">{{ $ministry->name_ar }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="file" class="col-md-4 col-form-label text-md-right">
                                    Fichier
                                </label>
                                <div class="col-md-8">
                                    <input id="file" type="file" name="file" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="file" class="col-md-4 col-form-label text-md-right">
                                    Créé le
                                </label>
                                <div class="col-md-8">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control datepicker pull-right" name="created_on">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="file" class="col-md-4 col-form-label text-md-right">
                                    Envoyé le
                                </label>
                                <div class="col-md-8">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control datepicker pull-right" name="sent_on">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="file" class="col-md-4 col-form-label text-md-right">
                                    Répondu le
                                </label>
                                <div class="col-md-8">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control datepicker pull-right" name="response_on">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-8 offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Ajouter
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    @if($request->gov_reqs)
                        <div class="panel-footer">
                            <ul class="list-group list-group-flush">
                                @foreach($request->gov_reqs as $gov_req)
                                    <li class="list-group-item"><a href="{{ route('show_gov_request', [$gov_req->id]) }}">{{ $gov_req->title }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            @if(\count($requestsByEmail) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading"
                         data-toggle="collapse"
                         href="#collapseLinkedByEmail"
                         role="button"
                         aria-expanded="true"
                         aria-controls="collapseLinkedByEmail">
                        Autres demandes Liées par e-mail
                    </div>
                    <div class="collapse  in show" id="collapseLinkedByEmail">
                        <div class="panel-body">
                            <ul class="list-group list-group-flush">
                                @foreach($requestsByEmail as $request_E)
                                    <li class="list-group-item">
                                        <a href="{{ route('show_admin_request',[$request_E->id]) }}"><i class="fa fa-eye"></i> {{ $request_E->title }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
            @if(\count($requestsByCin) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading"
                         data-toggle="collapse"
                         href="#collapseLinkedByCIN"
                         role="button"
                         aria-expanded="true"
                         aria-controls="collapseLinkedByCIN">
                        Autres demandes Liées par CIN
                    </div>
                    <div class="collapse  in show" id="collapseLinkedByCIN">
                        <div class="panel-body">
                            <ul class="list-group list-group-flush">
                                @foreach($requestsByCin as $request_C)
                                    <li class="list-group-item">
                                        <a href="{{ route('show_admin_request',[$request_C->id]) }}"><i class="fa fa-eye"></i> {{ $request_C->title }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
        </div>
		
		
		
		
		 <div class="col-md-6">
            @if($request->proof_file)
                <div style="margin-top: 15px; margin-bottom: 15px;">
                    <a href="{{ asset($request->proof_file) }}" class="btn btn-primary">Preuve de contact</a>
                </div>
            @endif

            <h1 style="font-size: 20px;">{{ $request->category->name }}</h1>
            <comment-component-admin
                :app_url="'{{url('/')}}'"
                :user="{{ auth()->user() }}"
                :update="'{{ (int) $update }}'"
                :request="{{ $request }}"
            ></comment-component-admin>
			
			
			
			 <file-component
                :app_url="'{{url('/')}}'"
                :user="{{ auth()->user() }}"
                :request="{{ $request }}"
                :update="{{ ($update == '1') ? true : false }}"
            ></file-component>
        </div>
		
		
		
		
		
		
		
		
		
    </div>

@endsection

@section('after_script')
    <!-- bootstrap datepicker -->
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
    <script>
        $(function () {
            //Date picker
            $('.datepicker').datepicker({
                autoclose: true,
                dateFormat: 'yyyy-mm-dd',
                format: 'yyyy-mm-dd'
            });
        });
    </script>
	<script type="text/javascript">
    window.onload = function () {
        //Reference the DropDownList.
        var ddlYears = document.getElementById("ddlYears");
 
        //Determine the Current Year.
        var currentYear = (new Date()).getFullYear();
 
        //Loop and add the Year values to DropDownList.
        for (var i = 2011; i <= currentYear; i++) {
            var option = document.createElement("OPTION");
            option.innerHTML = i;
            option.value = i;
			if(i==currentYear){
				option.selected = "selected";
			}
            ddlYears.appendChild(option);
        }
    };
    </script>
@endsection

