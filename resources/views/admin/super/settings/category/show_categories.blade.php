@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('section_title')
    Categories
@endsection

@section('content')

    @include('errors.errors')
    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif
    <table id="table" class="table table-striped table-bordered" style="width:100%">
        <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Name AR</th>
            <th>Name FR</th>
            <th>Name EN</th>
            <th>Active</th>
            <th style="width: 150px;">Created at</th>
            <th>Action</th>
        </tr>
        </thead>
    </table>
    <div class="text-right" style="margin-top: 20px;">
        <a href="{{ url('admin/settings/category/create') }}" class="btn btn-lg btn-primary">Add Category</a>
    </div>

@endsection

@section('after_script')

    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
				responsive: true,
                order: [0,'desc'],
                processing: true,
                serverSide: true,
                ajax: "{{ url('/api/admin/category_list') }}",
                columns: [
                    { "data": "id" },
                    { "data": "name_ar" },
                    { "data": "name_fr" },
                    { "data": "name_en" },
                    {
                        "data": "active",
                        "render": function(data){
                            if(data == 0){
                                return '<i class="fa fa-times-circle" title="Verified" style="font-size:20px; color:#e74c3c;"></i>'
                            }else{
                                return '<i class="fa fa-check-circle" title="Verified" style="font-size:20px; color:#badc58;"></i>';
                            }
                            return data;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "created_at",
                        "searchable": false,
                        "orderable":false,
                    },
                    {
                        "data": "action",
                        "searchable": false,
                        "orderable":false,
                    }
                ]
            });

        });

    </script>

@endsection
