@extends('admin.layouts.app')

@section('section_title')
    Edit Category
@endsection

@section('content')

    <div class="row">
        <div class="col-md-7">
            @include('errors.errors')
           <form method="POST" action="{{ route('update_category', ['category_id' =>$category->id]) }}"> 
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-md-5 col-form-label text-md-right">
                        {{ __('Category id') }} 
                 </label>
                       <div class="col-md-6">
                        <input id="name"
                               type="text"
                               class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}" 
                               name="category_id"
                               value="{{ $category->id }}" disabled="disabled" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-5 col-form-label text-md-right">
                       {{ __('Name_fr') }}  
                 </label>
                       <div class="col-md-6">
                        <input id="name"
                               type="text"
                                class="form-control{{ $errors->has('name_fr') ? ' is-invalid' : '' }}"
                               name="name_fr"
                               value="{{ $category->name_fr }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-5 col-form-label text-md-right">
                        {{ __('Name_ar') }} 
                 </label>
                       <div class="col-md-6">
                        <input id="name"
                               type="text"
                               class="form-control{{ $errors->has('name_ar') ? ' is-invalid' : '' }}"
                               name="name_ar"
                               value="{{ $category->name_ar }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-5 col-form-label text-md-right">
                        {{ __('Name_en') }} * 
                 </label>
                       <div class="col-md-6">
                        <input id="name"
                               type="text"
                               class="form-control{{ $errors->has('name_en') ? ' is-invalid' : '' }}"
                               name="name_en"
                               value="{{ $category->name_en }}" required>
                    </div>
                </div>
                <div class="form-group row" >
                    <label class="col-md-5 col-form-label text-md-right">Category active</label>
                    <div class="col-md-6">
                        <select name="active" class="form-control" required>
                            @if($category->active==1)
                                <option value="1" selected>Yes</option>
                                <option value="0">No</option>
                            @else
                                <option value="1">Yes</option>
                                <option value="0" selected>No</option>
                                
                        </select>
                    </div>
                </div>
              
                <div class="form-group row">
                    <div class="col-md-6 offset-5">
                        <button type="submit" class="btn btn-success">
                            {{ __('Update') }}
                        </button>
                    </div> 
                </form>
                <button type="submit" class="btn btn-danger"  onclick={{"if '(confirm(\'Are you sure want to update this item?\'))'
                {   return  null; }"}} disabled="disabled" >
                    {{ __('Delete') }}
                </button>

              
        </div>
    </div>

@endsection
