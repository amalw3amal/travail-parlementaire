@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.css" rel="stylesheet">
@endsection

@section('section_title')
    Add New Status
@endsection

@section('content')

    @include('errors.errors')
    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-8">
            <form action="{{ route('update_status' ,  ['status_id' =>$status->id]) }}" method="post">
                {{ csrf_field() }}
                {{($status->name)}}
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label text-md-right">{{__('Parent status')}}</label>
                    <div class="col-md-6">
                        <select name="parent_status" class="form-control">
                            <option value="">-- Select Parent --</option>
                            {{-- varible $status changed to $statue  --}}
                            @foreach($parent_status as $statue)
                                <option value="{{ $statue->id }}">{{ $statue->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label text-md-right">{{__('Name')}}</label>
                    <div class="col-md-6">
                        <input type="text" name="name" class="form-control" value="{{$status->name}}" required autofocus>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label text-md-right">{{__('Color: #')}}</label>
                    <div class="col-md-6">
                        <input type="text" id="simple-color-picker" name="color" class="form-control" value="{{ $status->color}}" required autofocus>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label text-md-right">{{__('Active')}}</label>
                    <div class="col-md-6">
                        <select name="active" class="form-control" required>
                            @if($status->active==1)
                            <option value="1" selected>Yes</option>
                            <option value="0">No</option>
                        @else
                            <option value="1">Yes</option>
                            <option value="0" selected>No</option>
                        @endif
                        </select>

                    </div>
                </div>
                <div class="col-md-8 offset-md-4">
                    <button class="btn btn-success">{{__('Update')}}</button>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('after_script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#simple-color-picker').colorpicker();
        });
    </script>
@endsection
