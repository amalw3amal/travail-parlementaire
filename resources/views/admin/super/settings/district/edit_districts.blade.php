@extends('admin.layouts.app')

@section('section_title')
    Edit district
@endsection

@section('content')

    <div class="row">
        <div class="col-md-7">
            @include('errors.errors')
           <form method="POST" action="{{ route('update_district', ['district_id' =>$district->id]) }}"> 
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-md-5 col-form-label text-md-right">
                        {{ __('district id') }} *   district id
                 </label>
                       <div class="col-md-6">
                        <input id="id"
                               type="text"
                               class="form-control{{ $errors->has('district_id') ? ' is-invalid' : '' }}"
                               name="name"
                               value="{{ old('name')?  old('name'):$district->id }}" disabled ="disabled" >
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-5 col-form-label text-md-right">
                        {{ __('District fr name') }} * 
                 </label>
                       <div class="col-md-6">
                        <input id="name"
                               type="text"
                               class="form-control{{ $errors->has('name_fr') ? ' is-invalid' : '' }}"
                               name="name_fr"
                               value="{{$district->name_fr }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-5 col-form-label text-md-right">
                        {{ __('district ar name') }} *   
                 </label>
                       <div class="col-md-6">
                        <input id="name"
                               type="text"
                               class="form-control{{ $errors->has('name_ar') ? ' is-invalid' : '' }}"
                               name="name_ar"
                               value="{{ old('name')?  old('name'):$district->name_ar }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-5 col-form-label text-md-right">
                        {{ __('District en name') }} *  
                 </label>
                       <div class="col-md-6">
                        <input id="name"
                               type="text"
                               class="form-control{{ $errors->has('name_en') ? ' is-invalid' : '' }}"
                               name="name_en"
                               value="{{$district->name_en }}" required>
                    </div>
                </div>
                <div class="form-group row" >
                    <label class="col-md-5 col-form-label text-md-right">District active</label>
                    <div class="col-md-6">
                        <select name="active" class="form-control" required>
                            @if($district->active==1)
                                <option value="1" selected>Yes</option>
                                <option value="0">No</option>
                            @else
                                <option value="1">Yes</option>
                                <option value="0" selected>No</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 offset-5">
                        <button type="submit" class="btn btn-success">
                            {{ __('Update') }}
                        </button>
                    </div> 
                </form>
                <button type="submit" class="btn btn-danger"  onclick={{"if (confirm(\'Are you sure want to update this item?\''))
                {return  null; }"  }}   disabled="disabled">
                    {{ __('Delete') }}
                </button>          
        </div>
    </div>

@endsection
