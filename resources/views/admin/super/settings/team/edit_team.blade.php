@extends('admin.layouts.app')

@section('section_title')
    Edit team
@endsection

@section('content')

    <div class="row">
        <div class="col-md-7">
            @include('errors.errors')
           <form method="POST" action="{{ route('update_team', ['team_id' =>$team->id]) }}"> 
                @csrf
                <div class="form-group row">
                    <label for="name" class="col-md-5 col-form-label text-md-right">
                      {{ __('team id') }}   
                 </label>
                       <div class="col-md-6">
                        <input id="name"
                               type="text"
                               class="form-control{{ $errors->has('team_id') ? ' is-invalid' : '' }}"
                               name="id"
                               value="{{ $team->id }}" disabled="disabled">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-5 col-form-label text-md-right">
                           {{__('team fr name')}}
                 </label>
                       <div class="col-md-6">
                        <input id="name"
                               type="text"
                               class="form-control{{ $errors->has('name_fr') ? ' is-invalid' : '' }}"
                               name="name_fr"
                               value="{{ $team->name_fr }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name_ar" class="col-md-5 col-form-label text-md-right">
                        {{ __(' team ar name') }} *  
                 </label>
                       <div class="col-md-6">
                        <input id="name"
                               type="text"
                               class="form-control{{ $errors->has('name_ar') ? ' is-invalid' : '' }}"
                               name="name_ar"
                               value="{{ $team->name_ar }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-5 col-form-label text-md-right">
                        {{ __('team en name') }} *   
                 </label>
                       <div class="col-md-6">
                        <input id="name"
                               type="text"
                               class="form-control{{ $errors->has('name_en') ? ' is-invalid' : '' }}"
                               name="name_en"
                               value="{{ $team->name_en }}" required>
                    </div>
                </div>
                <div class="form-group row" >
                    <label class="col-md-5 col-form-label text-md-right">Team active</label>
                    <div class="col-md-6">
                        
                        <select name="active" class="form-control" required>
                        @if($team->active==1)
                                <option value="1" selected>Yes</option>
                                <option value="0" >No</option>
                        @else
                                <option value="1" >Yes</option>
                                <option value="0" selected>No</option> 
                        @endif
                    </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 offset-5">
                        <button type="submit" class="btn btn-success">
                            {{ __('Update') }}
                        </button>
                    </div> 
                </form>
                <button type="submit" class="btn btn-danger"  onclick={{"if (confirm(\'Are you sure want to update this item?\''))
                {   return  null; }"
            }} disabled="disabled">
                    {{ __('Delete') }}
                </button>
      </div>
    </div>

@endsection
