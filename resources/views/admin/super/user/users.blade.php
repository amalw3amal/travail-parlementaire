@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('section_title')
    Admin Users
@endsection

@section('content')

    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif

    <table id="table" class="table table-striped table-bordered" style="width:100%">
        <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th style="width: 150px;">Created at</th>
            <th style="width: 150px;">Updated at</th>
            <th>Action</th>
        </tr>
        </thead>
    </table>

    <div class="row">
        <div class="col-md-12 text-right">
            <a href="{{ route('create_admin_user') }}" class="btn btn-primary">
                Add New User
            </a>
        </div>
    </div>

@endsection

@section('after_script')

    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
				responsive: true,
                order: [0,'desc'],
                processing: true,
                serverSide: true,
                ajax: "{{ url('/api/admin/admin_users_list') }}",
                columns: [
                    { "data": "id" },
                    { "data": "name" },
                    { "data": "email" },
                    {
                        "data": "role",
                        "searchable": false
                    },
                    {
                        "data": "created_at",
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "updated_at",
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "action",
                        "searchable": false,
                        "orderable":false
                    }
                ]
            });

        });
    </script>

@endsection
