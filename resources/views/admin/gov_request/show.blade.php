@extends('admin.layouts.app')

@section('content')

    @if($flash = session('message'))
        <div class="col-md-12">
            <div class="alert alert-success">
                <i class="fa fa-bell" aria-hidden="true"></i>
                {{ $flash }}
            </div>
        </div>
    @endif

    <div class="col-md-7">
        <div style="margin-bottom: 20px;">
            <ul>
                <li>
                    Créé par <b style="text-decoration: underline">{{ $req->user->name }}</b>, le {{ $req->created_at }}
                </li>
                @if($req->updated_at != $req->created_at)
                    <li>
                        Updated On {{ $req->updated_at }}
                    </li>
                @endif
            </ul>
            <h1>{{ $req->title }}</h1>
            <table class="table table-bordered" style="font-size: 16px;">
                <tr>
                    <th width="20%">Type Request</th>
                    <td>{{ \Illuminate\Support\Facades\Lang::get('layout.'.$req->request_type) }}</td>
                </tr>
                <tr>
                    <th>Numéro de correspondance</th>
                    <td>{{ $req->request_unique_number }}</td>
                </tr>
                @if($req->district)
                <tr>
                    <th>Région</th>
                    <td>{{ $req->district->name_fr }}</td>
                </tr>
                @endif
                @if($req->category)
                <tr>
                    <th>Catégorie</th>
                    <td>{{ $req->category->name_fr }}</td>
                </tr>
                @endif
                @if($req->ministry)
                <tr>
                    <th>Ministère</th>
                    <td>{{ $req->ministry->name_fr }}</td>
                </tr>
                @endif
                <tr>
                    <th>Créé le</th>
                    <td>{{ \Carbon\Carbon::create($req->created_on)->format('Y-m-d') }}.</td>
                </tr>
                <tr>
                    <th>Envoyé le</th>
                    <td>{{ \Carbon\Carbon::create($req->sent_on)->format('Y-m-d') }}.</td>
                </tr>
                <tr>
                    <th>Répondu le</th>
                    <td>{{ \Carbon\Carbon::create($req->response_on)->format('Y-m-d') }}.</td>
                </tr>
                <tr>
                    <th colspan="2">Description</th>
                </tr>
                <tr>
                    <td colspan="2">{{ $req->description }}</td>
                </tr>
            </table>
            <a href="{{ asset($req->file) }}" target="_blank" class="btn btn-info text-white">
                <i class="fa fa-file" aria-hidden="true"></i>
                {{ $req->file_name }}
            </a>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <ul>
                    @foreach($req->comments as $comment)
                        <li style="margin-bottom: 10px;">
                            <b style="text-decoration: underline">{{ $comment->user->name }}</b>:
                            <span style="font-size: 18px;">{{ $comment->body }}</span>
                            @if($comment->file)
                                <a href="{{ asset($comment->file) }}"
                                   target="_blank"
                                   class="btn btn-info text-white">
                                    <i class="fa fa-file" aria-hidden="true"></i>
                                    {{ $comment->file_name }}
                                </a>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
            @can('gov_request.is_edit')
                <div class="panel-footer">
                    <form method="post"
                          action="{{ route('gov_request_post_comment',['gov_request_id' => $req->id])}}"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="gov_request_id">
                        @include('errors.errors')
                        <div class="row form-group">
                            <label class="col-md-3 text-right">Commentaire</label>
                            <div class="col-md-8">
                                <textarea class="form-control" name="body"></textarea>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 text-right">Fichier</label>
                            <div class="col-md-8">
                                <input type="file" name="file">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-8 offset-3">
                                <input type="submit" class="btn btn-primary" value="Envoyer commentaire">
                            </div>
                        </div>
                    </form>
                </div>
            @endcan
        </div>
    </div>
    <div class="col-md-5">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Demande(s)</h2>
            </div>
            <div class="card-body">
                <ul class="list-group list-group-flush">
                    @foreach($req->reqs as $r)
                        <li class="list-group-item"><a href="{{ route('show_admin_request', ['id'=>$r->id]) }}">{{ $r->title }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

@endsection
