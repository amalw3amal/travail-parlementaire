@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('section_title')
    Correspendances avec le gouvernement
@endsection

@section('content')


    <div class="card-body">
        <form method="get" class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Numéro de correspondance</label>
                    <input type="text" class="form-control" name="number" value="{{ isset($reqs['number']) ? $reqs['number'] : '' }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Utilisateur</label>
                    <input type="text" class="form-control" name="name" value="{{ isset($reqs['name']) ? $reqs['name'] : '' }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Titre</label>
                    <input type="text" class="form-control" name="title" value="{{ isset($reqs['title']) ? $reqs['title'] : '' }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Categorie</label>
                    <select class="form-control" name="category">
                        <option>--</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}"  {{ isset($reqs['category'])?$reqs['category']==$category->id?'selected':'':'' }}>{{ $category->name_fr }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Ministère</label>
                    <select class="form-control" name="ministry">
                        <option>--</option>
                        @foreach($ministries as $ministry)
                            <option value="{{ $ministry->id }}" {{ isset($reqs['ministry'])?$reqs['ministry']==$ministry->id?'selected':'':'' }}>{{ $ministry->name_fr }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group col-md-12">
                <button class="btn btn-primary" type="submit">Rechercher</button>
            </div>
        </form>
        <hr>
    </div>

    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif

    <table id="table" class="table table-striped table-hover table-bordered" style="width:100%">
        <thead class="thead-dark">
        <tr>
            <th>Numéro de correspondance</th>
            <th>Utiliseur</th>
            <th>Titre</th>
            <th>Catégorie</th>
            <th>Ministère</th>
            <th style="width: 150px;">Créé le</th>
            <th>Action</th>
        </tr>
        </thead>
    </table>

    <div class="row" style="margin-top: 20px;">
        <div class="col-md-12 text-right">
            <a href="{{ route('create_gov_request') }}" class="btn btn-primary">Ajouter</a>
        </div>
    </div>

@endsection

@section('after_script')

    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
				responsive: true,				
                order: [0,'desc'],
                processing: true,
                serverSide: true,
				language: {
				url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/French.json"
				},
                ajax: {
                    url: "{{ url('/api/admin/gov_requests_list') }}",
                    data: {
                        number: '{{ isset($reqs['number'])?$reqs['number']:null }}',
                        name: '{{ isset($reqs['name'])?$reqs['name']:null }}',
                        title: '{{ isset($reqs['title'])?$reqs['title']:null }}',
                        category: '{{ isset($reqs['category'])?$reqs['category']:null }}',
                        ministry: '{{ isset($reqs['ministry'])?$reqs['ministry']:null }}',
                    }
                },
                columns: [
                    { "data": "request_unique_number" },
                    { "data": "user" },
                    { "data": "title" },
                    { "data": "category" },
                    { "data": "ministry" },
                    {
                        "data": "created_at",
                        "searchable": false,
                        "orderable":false
                    },
                    { "data": "action" }
                ]
            });

        });
    </script>

@endsection
