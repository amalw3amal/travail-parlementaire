@extends('admin.layouts.app')

@section('after_style')

    <!-- bootstrap datepicker -->
    <link rel="stylesheet"
          href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">

    <style>
        .remove:hover {
            opacity: .5;
            cursor: pointer;
        }
    </style>

@endsection

@section('section_title')
    Correspendances avec le gouvernement
@endsection

@section('content')

    <div class="box-header">
        <h3 class="box-title">Modifier Correspendance</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                @include('errors.errors')
                <form method="POST" action="{{ route('update_gov_request', [$req->id]) }}"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label for="request_type" class="col-md-4 col-form-label text-md-right">
                            Type *
                        </label>
                        <div class="col-md-8" id="request_type">
                            <select name="request_type" required class="form-control">
                                <option value="">Selectionner un type</option>
                                <option
                                    value="Written_question" {{ $req->request_type == 'Written_question' ? 'selected' : '' }}>
                                    سؤال كتابي
                                </option>
                                <option
                                    value="Oral_question" {{ $req->request_type == 'Oral_question' ? 'selected' : '' }}>
                                    سؤال شفاهي
                                </option>
                                <option
                                    value="Direct_messaging" {{ $req->request_type == 'Direct_messaging' ? 'selected' : '' }}>
                                    مراسلة مباشرة
                                </option>
                                <option
                                    value="Answer_to_a_written_question" {{ $req->request_type == 'Answer_to_a_written_question' ? 'selected' : '' }}>
                                    جواب على سؤال كتابي
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="request_unique_number" class="col-md-4 col-form-label text-md-right">
                            Numéro de correspondance *
                        </label>
                        <div class="col-md-8">
                            <input id="request_unique_number" type="text" disabled readonly value="{{ $req->request_unique_number }}"
                                   class="form-control" name="request_unique_number" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="title" class="col-md-4 col-form-label text-md-right">
                            Titre *
                        </label>
                        <div class="col-md-8">
                            <input id="title" type="text" class="form-control" name="title" value="{{ $req->title }}"
                                   required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-md-4 col-form-label text-md-right">
                            Description *
                        </label>
                        <div class="col-md-8">
                            <textarea id="description" name="description" class="form-control"
                                      required>{{ $req->description }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right" required="required">
                            Demande(s) *
                        </label>
                        <div class="col-md-8">
                            <select class="form-control select2" name="requests[]" multiple="multiple" required>
                                @foreach($requests as $request)
                                    <option value="{{ $request->id }}"
                                            @foreach($req->reqs as $r)
                                                @if($request->id == $r->id)
                                                    selected
                                                    @break
                                                @endif
                                            @endforeach
                                    >{{ $request->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">
                            Région *
                        </label>
                        <div class="col-md-8">
                            <select class="form-control" name="district" required>
                                <option value="">--</option>
                                @foreach($districts as $district)
                                    <option
                                        value="{{ $district->id }}" {{ $district->id == $req->district_id ? 'selected' : '' }}>{{ $district->name_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">
                            Catégorie *
                        </label>
                        <div class="col-md-8">
                            <select class="form-control" name="category" required>
                                <option value="">--</option>
                                @foreach($categories as $category)
                                    <option
                                        value="{{ $category->id }}" {{ $category->id == $req->category_id ? 'selected' : '' }}>{{ $category->name_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">
                            Ministère *
                        </label>
                        <div class="col-md-8">
                            <select class="form-control" required name="ministry">
                                <option value="">--</option>
                                @foreach($ministries as $ministry)
                                    <option
                                        value="{{ $ministry->id }}" {{ $ministry->id == $req->ministry_id ? 'selected' : '' }}>{{ $ministry->name_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="file" class="col-md-4 col-form-label text-md-right">
                            Fichier
                        </label>
                        <div class="col-md-8">
                            <div id="existedFile"
                                 @if(!$req->file)
                                 style="display:none;"
                                @endif
                            >
                                <li class="list-group-item">
                                    <small class="pull-right">
                                        <div title="remove" class="remove" id="removeFile">
                                            <i aria-hidden="true" class="fa fa-times"></i>
                                        </div>
                                    </small>
                                    <a href="{{ asset($req->file) }}" target="_blank">
                                        <i class="fa fa-file" aria-hidden="true"></i>
                                        {{ $req->file_name }}
                                    </a>
                                </li>
                            </div>
                            <div id="selectFile"
                                 @if($req->file)
                                 style="display:none;"
                                @endif
                            >
                                <input id="file" type="file" name="file">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="file" class="col-md-4 col-form-label text-md-right">
                            Créé le
                        </label>
                        <div class="col-md-8">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control datepicker pull-right"
                                       value="{{ $req->created_on ? (new DateTime($req->created_on))->format('Y-m-d') : '' }}"
                                       name="created_on">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="file" class="col-md-4 col-form-label text-md-right">
                            Envoyé le
                        </label>
                        <div class="col-md-8">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control datepicker pull-right"
                                       value="{{ $req->sent_on ? (new DateTime($req->sent_on))->format('Y-m-d') : '' }}"
                                       name="sent_on">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="file" class="col-md-4 col-form-label text-md-right">
                            Répondu le
                        </label>
                        <div class="col-md-8">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control datepicker pull-right"
                                       value="{{ $req->response_on ? (new DateTime($req->response_on))->format('Y-m-d') : '' }}"
                                       name="response_on">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-8 offset-4">
                            <input type="submit" class="btn btn-primary" value="Enregistrer">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('after_script')

    <!-- bootstrap datepicker -->
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();
            //Date picker
            $('.datepicker').datepicker({
                autoclose: true,
                dateFormat: 'yyyy-mm-dd',
                format: 'yyyy-mm-dd'
            });

        });

        $('#removeFile').click(function () {
            $('#existedFile').css('display', 'none');
            $('#selectFile').css('display', 'block');
        })
    </script>

@endsection
