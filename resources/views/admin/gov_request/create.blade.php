@extends('admin.layouts.app')

@section('after_style')

    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">

@endsection

@section('section_title')
    Gov Request
@endsection

@section('content')

  @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif

    <div class="box-header">
        <h3 class="box-title">Ajout Correspendance avec le gouvernement</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                @include('errors.errors')
                <form method="POST" action="{{ route('store_gov_request') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label for="request_type" class="col-md-4 col-form-label text-md-right">
                            Type *
                        </label>
                        <div class="col-md-8" id="request_type">
                            <select name="request_type" required class="form-control">
                                <option value="">Selectionner un type</option>
                                <option value="Written_question">سؤال كتابي</option>
                                <option value="Oral_question">سؤال شفاهي</option>
                                <option value="Direct_messaging">مراسلة مباشرة</option>
                                <option value="Answer_to_a_written_question">جواب على سؤال كتابي</option>
                            </select>
                        </div>
                    </div>
					
					

                    <div class="form-group row">
                        <label for="request_unique_number" class="col-md-4 col-form-label text-md-right">
                           Numéro de correspondance *
                        </label>
                        <div class="col-md-8">
                            <input id="request_unique_number" type="number" class="form-control" name="request_unique_number" required>
                        </div>
                    </div>
					<div class="form-group row">
                        <label for="request_unique_number" class="col-md-4 col-form-label text-md-right">
                           Année *
                        </label>
                        <div class="col-md-8">
                            <select id="ddlYears" class="form-control" name="annee" required>></select>
                        </div>
                    </div>
					
					
                    <div class="form-group row">
                        <label for="title" class="col-md-4 col-form-label text-md-right">
                            Titre *
                        </label>
                        <div class="col-md-8">
                            <input id="title" type="text" class="form-control" name="title" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-md-4 col-form-label text-md-right">
                            Description *
                        </label>
                        <div class="col-md-8">
                            <textarea id="description" name="description" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right" required>
                            Demande(s) *
                        </label>
                        <div class="col-md-8">
                            <select class="form-control select2" name="requests[]" multiple="multiple" required>
                                @foreach($requests as $request)
                                    <option value="{{ $request->id }}">{{ $request->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">
                            Région *
                        </label>
                        <div class="col-md-8">
                            <select class="form-control" name="district" required>
                                <option value="">--</option>
                                @foreach($districts as $district)
                                    <option value="{{ $district->id }}">{{ $district->name_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">
                            Catégorie *
                        </label>
                        <div class="col-md-8">
                            <select class="form-control" name="category" required>
                                <option value="">--</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">
                            Ministère *
                        </label>
                        <div class="col-md-8">
                            <select class="form-control" required name="ministry">
                                <option value="">--</option>
                                @foreach($ministries as $ministry)
                                    <option value="{{ $ministry->id }}">{{ $ministry->name_fr }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="file" class="col-md-4 col-form-label text-md-right">
                            Fichier
                        </label>
                        <div class="col-md-8">
                            <input id="file" type="file" name="file" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="file" class="col-md-4 col-form-label text-md-right">
                            Créé le
                        </label>
                        <div class="col-md-8">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control datepicker pull-right" name="created_on">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="file" class="col-md-4 col-form-label text-md-right">
                            Envoyé le
                        </label>
                        <div class="col-md-8">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control datepicker pull-right" name="sent_on">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="file" class="col-md-4 col-form-label text-md-right">
                            Répondu le
                        </label>
                        <div class="col-md-8">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control datepicker pull-right" name="response_on">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-8 offset-4">
                            <button type="submit" class="btn btn-primary">
                                Ajouter
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('after_script')

    <!-- bootstrap datepicker -->
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();
            //Date picker
            $('.datepicker').datepicker({
                autoclose: true,
                dateFormat: 'yyyy-mm-dd',
                format: 'yyyy-mm-dd'
            });

        });
    </script>
	<script type="text/javascript">
    window.onload = function () {
        //Reference the DropDownList.
        var ddlYears = document.getElementById("ddlYears");
 
        //Determine the Current Year.
        var currentYear = (new Date()).getFullYear();
 
        //Loop and add the Year values to DropDownList.
        for (var i = 2011; i <= currentYear; i++) {
            var option = document.createElement("OPTION");
            option.innerHTML = i;
            option.value = i;
			if(i==currentYear){
				option.selected = "selected";
			}
            ddlYears.appendChild(option);
        }
    };
    </script>

@endsection
