@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('section_title')
    {{ $applicant->name }}
@endsection

@section('content')

    <div class="row">
        <div class="col-md-3">
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><b>CIN</b>: {{ $applicant->cin }}</li>
                <li class="list-group-item"><b>E</b>: <a href="mailto:{{ $applicant->email }}">{{ $applicant->email }}</a></li>
                <li class="list-group-item"><b>T</b>: {{ $applicant->phone_number }}</li>
            </ul>
        </div>
        <div class="col-md-9">
            <div class="card shadow-sm">
                <div class="card-header">
                    <h2 class="card-title">Demandes</h2>
                </div>
                <div class="card-body">

                    @if($flash = session('message'))
                        <div class="alert alert-success">
                            <i class="fa fa-bell" aria-hidden="true"></i>
                            {{ $flash }}
                        </div>
                    @endif

                    <table id="table" class="table  table-light" style="width:100%">
                        <thead class="thead-dark">
                        <tr>
                            <th>Title</th>
                            <th>District</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th style="width: 150px;">Updated at</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('after_script')

    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
				responsive: true,			
                order: [0,'desc'],
                processing: true,
                serverSide: true,
				language: {
				url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/French.json"
				},
                ajax: "{{ url('/api/admin/requests_list/'.$applicant->id) }}",
                columns: [
                    { "data": "title" },
                    {
                        "data": "district",
                        "render": function(data){
                            return data.name_en;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "category",
                        "render": function(data){
                            return data.name_en;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "status",
                        "render": function(data){
                            if(data != null){
                                return '<i class="fa fa-circle" style="font-size:20px; color:#' + data.color + ';"></i>'
                            }else{
                                return '<i class="fa fa-circle" style="font-size:20px; color:#757575;"></i> Waiting'
                            }
                            return data;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "updated_at",
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "action",
                        "searchable": false,
                        "orderable":false
                    }
                ]
            });
        });
    </script>

@endsection
