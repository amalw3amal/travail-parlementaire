@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/responsive/2.2.7/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('section_title')
    Demandeurs
@endsection

@section('content')


    <div class="card-body">
        <form method="get" class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>CIN</label>
                    <input type="text" class="form-control" name="cin" value="{{ isset($req['cin']) ? $req['cin'] : '' }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Nom</label>
                    <input type="text" class="form-control" name="name" value="{{ isset($req['name']) ? $req['name'] : '' }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" value="{{ isset($req['email']) ? $req['email'] : '' }}">
                </div>
            </div>
            <div class="form-group col-md-12">
                <button class="btn btn-primary" type="submit">Rechercher</button>
            </div>
        </form>
        <hr>
    </div>

    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif

    <table id="table" class="table table-light" style="width:100%">
        <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>CIN</th>
                <th>Nom</th>
                <th>Email</th>
                <th style="width: 150px;">Créé le</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>

@endsection

@section('after_script')

    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
				responsive: true,
                order: [0, 'desc'],
                processing: true,
                serverSide: true,
				bFilter: false,
				language: {
				url: "https://cdn.datatables.net/plug-ins/1.10.24/i18n/French.json"
				},
                ajax: {
                    url: "{{ url('/api/admin/users_list') }}",
                    data: {
                        cin: '{{ isset($req['cin'])?$req['cin']:null }}',
                        name: '{{ isset($req['name'])?$req['name']:null }}',
                        email: '{{ isset($req['email'])?$req['email']:null }}',
                    }
                },
                columns: [
                    {
                        "data": "id",
                        "searchable": true,
                        "orderable": true
                    },
                    {
                        "data": "cin",
                        "searchable": true,
                        "orderable": true
                    },
                    {
                        "data": "name",
                        "searchable": true,
                        "orderable": true
                    },
                    {
                        "data": "email",
                        "searchable": true,
                        "orderable": true
                    },
                    {
                        "data": "created_at",
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "action",
                        "searchable": false,
                        "orderable":false
                    }
                ]
            });
        });
    </script>
@endsection
