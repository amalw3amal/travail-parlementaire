@extends('layouts.app')
@section('content')
    <p id="myText" style="display: none">@lang('layout.select_district')</p>
    <p id="Local" style="display: none">{{ app()->getLocale() }}</p>
    <input type="hidden" name="recaptcha" id="recaptcha" value="">
    {{-- Begin Start --}}
    <section class="slide" v-if="startRequest == false">
        <div class="home">
            <div class="container">
                <div class="row justify-content-center p-4 p-md-5">
                    <div class="card shadow col-md-6  text-white" style="border-color: transparent;background-color: rgba(108, 117, 125, 0.82);padding: .75rem 1.25rem;">
                        <h2 class="font-italic text-white">@lang('layout.intro')</h2>
                        <p class="lead my-3">@lang('layout.intro_description_1')</p>
                    </div>
                    <div class="col-md-6">
                        <div class="card shadow">
                            <div class="card-header">
                                <h2><i>@lang('layout.make_application')</i></h2>
                            </div>
                            <div class="card-body">
                                <p>@lang('layout.make_app_description')</p>
                                <div>
                                    <div class="form-group">
                                        <select class="form-control form-control-lg" v-model="data.district">
                                            <option value="null">@lang('layout.select_your')</option>
                                            @foreach($districts as $district)
                                                <option value="{{ $district->id }}">
                                                    @if(app()->getLocale() == 'ar')
                                                        {{ $district->name_ar }}
                                                    @elseif(app()->getLocale() == 'fr')
                                                        {{ $district->name_fr }}
                                                    @else
                                                        {{ $district->name_en }}
                                                    @endif
                                                </option>
                                            @endforeach
                                        </select>
                                        <button type="submit" @click="start_function()"
                                                class="btn btn-lg btn-block btn-primary">
                                            @lang('layout.create_request')
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- End Start --}}
    <section class="container" v-if="startRequest != false"
             @if(app()->getLocale() == 'ar')style="text-align: right"@endif>
        <div class="card" style="border: 2px dashed #aaa">
            <div class="card-body" v-if="accepte == true">
                {{-- Begin select Disrict --}}
                <div class="col-md-10">
                    <h2>@lang('layout.make_application')</h2>
                    <hr style="opacity: .5">

                    <div class="form-group row" v-if="loading == false && message.length < 1">
                        <label for="start_district"
                               class="col-md-4 col-form-label text-md-right">@lang('layout.district')</label>
                        <div class="col-md-5">
                            <select class="form-control form-control-lg" id="start_district" @change="load_deputes()"
                                    v-model="data.district">
                                @foreach($districts as $district)
                                    <option value="{{ $district->id }}">
                                        @if(app()->getLocale() == 'ar')
                                            {{ $district->name_ar }}
                                        @elseif(app()->getLocale() == 'fr')
                                            {{ $district->name_fr }}
                                        @else
                                            {{ $district->name_en }}
                                        @endif
                                    </option>
                                @endforeach
                            </select>
                            <p style="margin-top: 15px;">@lang('layout.make_app_description')</p>
                        </div>
                    </div>
                </div>
                {{-- End select Disrict --}}

                @if(config('app.requested_prof') == true)
                    {{-- Begin Profiles --}}
                    <div class="col-md-10"
                         v-if="deputes.length == 0 && profiles.length > 0 && loading == false && message.length < 1">
                        <h3>
                            @lang('layout.our_deputies_at')
                        </h3>
                        <div class="alert alert-info">
                            <p>@lang('layout.p1'), <br>@lang('layout.p2')</p>
                        </div>
                        {{-- Begin profiles ---}}
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-6" v-for="profile in profiles">
                                <div class="card shadow-sm">
                                    <img class="card-img-top" :src="profile.avatar">
                                    <div class="card-body" style="padding-left: 5px; padding-right: 5px;">
                                        <h3 style="font-family: 'poppins', sans-serif; text-align: left">
                                            @if(app()->getLocale() == 'ar')
                                                @{{ profile.name_ar }}
                                            @elseif(app()->getLocale() == 'fr')
                                                @{{ profile.name_fr }}
                                            @else
                                                @{{ profile.name_en }}
                                            @endif
                                        </h3>
                                        <ul style="text-align: left">
                                            <li style="text-align: left"><a :href="'mailto:'+profile.email">@{{
                                                    profile.email }}</a></li>
                                            <li style="text-align: left"><a :href="'tel:'+profile.phone_number">@{{
                                                    profile.phone_number }}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- End profiles --}}
                        <h3>
                            @lang('layout.proof_of_contact')
                        </h3>
                        <hr style="opacity: .5">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">@lang('layout.join_files')</label>
                            <div class="col-md-5">
                                <label for="file" class="btn btn-dark">Transférer des fichiers</label>
                                <input type="file"
                                       name="proof_file"
                                       @change="selected_file = true" accept=".doc, .docx, .pdf, .png, .jpeg, .jpg" id="file" class="d-none">
                            </div>
                        </div>
                    </div>
                    {{-- End Profiles --}}
                @endif

                <div class="col-md-10"
                     @if(config('app.requested_prof') == true)
                     v-if="((deputes.length > 0 ) || (selected_file == true)) && loading == false && message.length < 1"
                     @else
                     v-if="loading == false && message.length < 1"
                    @endif
                >
                    <h3>@lang('layout.tell_us_about_you')</h3>
                    <hr style="opacity: .5">
                    <div class="form-group row">
                        <label for="cin" class="col-md-4 col-form-label text-md-right">@lang('layout.id')</label>
                        <div class="col-md-5">
                            <input id="cin" type="number" v-model="data.cin" class="form-control form-control-lg">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">@lang('layout.name')</label>
                        <div class="col-md-5">
                            <input id="name" type="text" v-model="data.name" class="form-control form-control-lg">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="gender" class="col-md-4 col-form-label text-md-right">@lang('layout.sex')</label>
                        <div class="col-md-5">
                            <select class="form-control form-control-lg" v-model="data.gender" id="gender">
                                <option>--</option>
                                <option value="Male">@lang('layout.male')</option>
                                <option value="Female">@lang('layout.female')</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone_number" class="col-md-4 col-form-label text-md-right">
                            @lang('layout.phone_number')
                        </label>
                        <div class="col-md-5" style="direction: ltr!important;">
                            <vue-tel-input
                                @validate="getPayload"
								placeholder=""
                                v-model="data.phone_number"></vue-tel-input>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">
                            @lang('layout.email')
                        </label>
                        <div class="col-md-5">
                            <input id="email" type="email" v-model="data.email" class="form-control form-control-lg"
                                   name="email" style="direction: ltr!important;">
                        </div>
                    </div>
                    <h3>@lang('layout.tell_us_about_request')</h3>
                    <hr style="opacity: .5">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">@lang('layout.category')</label>
                        <div class="col-md-5">
                            <select class="form-control form-control-lg" v-model="data.category" required
                                    name="category">
                                <option>**</option>
                                @foreach($categories as $category)
                                    <option
                                        value="{{ $category->id }}" {{ old('category')? old('category')==$category->id?'selected':'':'' }}>
                                        @if(app()->getLocale() == 'fr')
                                            {{ $category->name_fr }}
                                        @elseif(app()->getLocale() == 'ar')
                                            {{ $category->name_ar }}
                                        @else
                                            {{ $category->name_en }}
                                        @endif
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="title" class="col-md-4 col-form-label text-md-right">@lang('layout.title')</label>
                        <div class="col-md-5">
                            <input class="form-control form-control-lg" v-model="data.title" id="title">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description"
                               class="col-md-4 col-form-label text-md-right">@lang('layout.description')</label>
                        <div class="col-md-8">
                            <textarea class="form-control form-control-lg" v-model="data.description"
                                      id="description"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="join_files" class="col-md-4 col-form-label text-md-right">
                            @lang('layout.join_files')
                        </label>
                        <div class="col-md-5">
                            <input type="file" @change="onFileChange" accept=".doc, .docx, .pdf, .png, .jpeg, .jpg" multiple id="join_files">
                        </div>
                    </div>
                    <hr style="opacity: .5">
                    <input type="hidden" id="g-recaptcha-response" value="" name="g-recaptcha-response">
                    <validation-errors
                        :errors="validationErrors"
                        v-if="validationErrors"></validation-errors>
                    <div class="form-group row">
                        <div class="col-md-8 offset-md-4">
                            <button class="btn btn-lg btn-primary" @click="post_request()">
                                @lang('layout.create_request')
                            </button>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                var isFacebookApp = function () {
                   var ua = navigator.userAgent || navigator.vendor || window.opera;
                   return (ua.indexOf("FBAN") > -1) || (ua.indexOf("FBAV") > -1 ) || (ua.indexOf("Instagram") > -1);
                };

                if (isFacebookApp()) {
                    document.getElementById("join_files").removeAttribute("accept");
                }
                </script>

                <div class="col-md-10" v-if="loading == true">
                    @lang('layout.loading')
                </div>

                <div class="col-md-10" v-if="message.length > 0 && uploading_files == false">
                    <div class="alert alert-success">
                        <p style="font-size: 25px;" v-html="message"></p>
                    </div>
                </div>
                <div class="col-md-10" v-if="message.length > 0 && uploading_files == true">
                    <div class="alert alert-success">
                        <p style="font-size: 25px;">
                            <span>جاري تحميل الملفات المرافقة</span>
                        </p>
                    </div>
                </div>

            </div>
            <div class="card-body" v-if="accepte == false">
                <div class="row">
                    <div class="col-md-8">
                        <p style="font-size: 20px;">
                            @lang('layout.roles').
                        </p>
                        <div>
                            <button class="btn btn-lg btn-primary"
                                    @click="accepte = true">@lang('layout.accepte')</button>
                            <button class="btn btn-lg btn-warning"
                                    @click="startRequest = false">@lang('layout.refuse')</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section style="padding-top: 50px; padding-bottom: 50px; background-color: #252525">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-10 offset-1 text-center">
                    <p style="color: #fff; font-size: 20px;">
                        @lang('layout.text_gov_requested')
                    </p>
                    {{--
                    <a href="{{ route('public_gov_requests', [app()->getLocale()]) }}">
                        <button style="padding: 15px 50px; font-size: 20px;" class="btn btn-primary">@lang('layout.gov_requested')</button>
                    </a>
                    --}}
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-4 pt-4 pb-4">
                    <a href="https://www.ansi.tn" target="_blank">
                        <img src="{{ asset('images/logos/ansi-logo.png') }}" class="img-responsive" style="width: 100%;">
                    </a>
                </div>
                <div class="col-md-4" style="background-color: rgb(37, 37, 37);">
                    <a href="https://www.keystone.tn" target="_blank">
                        <img src="{{ asset('images/logos/keystone-logo.png') }}" class="img-responsive" style="width: 100%;">
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="http://www.inpdp.nat.tn" target="_blank">
                        <img src="{{ asset('images/logos/inpdp-logo.jpg') }}" class="img-responsive" style="width: 100%;">
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('script')
    @if(config('app.active_recaptcha') === true)
        <script src="https://www.google.com/recaptcha/api.js?render={{ env('SITE_KEY') }}"></script>
        <script>
            window.grecaptcha.ready(function () {
                window.grecaptcha.execute('{{ env('SITE_KEY') }}', {action: 'contact'})
                    .then(function (token) {
                        document.getElementById('recaptcha').value = token;
                    });
            });
        </script>
    @endif
@endpush
