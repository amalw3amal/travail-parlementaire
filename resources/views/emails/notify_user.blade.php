<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:v="urn:schemas-microsoft-com:vml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title></title>
    <style>
        body,
        html {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
			direction: rtl;
}
        a {
            text-decoration: none;
        }
    </style>
</head>

<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td></td>
        <td width="800">
            <div align="center" style="padding: 0">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" style="max-width:800px;@if(app()->getLocale() == 'ar')
                    direction: rtl;
                @endif">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width : 100%;">
                                <tr>
                                    <td style="text-align:center;">
                                        <img src="https://question.mouvementamal.tn/images/logoAmal.png">
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width : 100%;">
                                <tr>
                                    <td style="text-align:center;">
                                        <table width="92%"
                                               style="margin:auto; margin-top:10px; width:92%; max-width:800px;">
                                            <tr>
                                                <td style=";color:#050033; ">@lang('layout.notify_user_title',[],'ar')</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <hr style="color:#050033;"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:20px 0px 40px 0px;">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td style="border:1px solid #ffffff;">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">

                                            <tr>
                                                <td style="font-family:Verdana; color:#050033; font-size:16px; vertical-align:top; padding-top:15px;">

                                                    @lang('layout.notify_user_body', [
                                                            'key'       => $key,
                                                            'cin'       => $cin,
															'title'       => $title,
															'name'       => $name
                                                        ],'ar')

                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: #84162F;text-align: center;">
                        </td>
                    </tr>
                    <tr>
                        <td style="color:#ffffff; background-color:#84162F; text-align:center; font-size:13px; padding:20px;">
                            <a style="color:#ffffff; background-color:#84162F; text-align:center; font-size:13px; padding:20px;"
                               href="https://question.mouvementamal.tn/">https://question.mouvementamal.tn
                            </a>
                        </td>
                    </tr>

                </table>
            </div>
        </td>
        <td></td>
    </tr>
</table>
</body>

</html>
