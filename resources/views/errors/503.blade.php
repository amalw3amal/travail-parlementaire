@extends('layouts.app_errors')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>@lang('layout.page_503')</h1>
                <h2>@lang('layout.not_found_503')</h2>
                <ul>
                    
                </ul>
            </div>
        </div>
    </div>
@endsection
