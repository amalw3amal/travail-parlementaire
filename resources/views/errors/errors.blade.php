@if(count($errors))
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <div  role="alert">
                {{ $error }}
              </div>
            @endforeach
        </ul>
    </div>
@endif
