/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');
/*import Vue from 'vue'
window.Vue = Vue;
require('./bootstrap'); 
*/
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


import VueTelInput from 'vue-tel-input';
import VueRecaptcha from 'vue-recaptcha';
//FO
Vue.component('comment-component', require('./components/Comment.vue')/*.default*/);
Vue.component('comment-component-admin', require('./components/CommentAdmin.vue')/*.default*/);
Vue.component('file-component', require('./components/File.vue')/*.default*/);
Vue.component('vue-recaptcha', VueRecaptcha)

//BO
Vue.component('roles-component', require('./components/Roles.vue')/*.default*/);
Vue.component('edit-roles-component', require('./components/EditRoles.vue')/*.default*/);
Vue.component('date-select', require('./components/DateSelect')/*.default*/);
Vue.component('vue-tel-input', VueTelInput);

Vue.component('validation-errors', {
    data() {
        return {}
    },
    props: ['errors'],
    template: `
        <div v-if="validationErrors">
            <ul class="alert alert-danger">
                <li v-for="(value, key, index) in validationErrors">- {{ value }}</li>
            </ul>
        </div>`,
    computed: {
        validationErrors() {
            let errors = Object.values(this.errors);
            errors = errors.flat();
            return errors;
        }
    }
});

Vue.use(VueTelInput);

const app = new Vue({
    el: '#app',
    data: {
        startRequest: false,
        district: null,
        accepte: false,
        deputes: [],
        profiles: [],
        selected_file: false,
        validationErrors: '',
        data: {
            district: null,
            proof_of_contact: null,
            cin: null,
            name: null,
            gender: null,
            phone_number: null,
            phone: null,
            email: null,
            category: null,
            title: null,
            description: [],
            recaptcha: null
        },
        files: [],
        file_to_upload: [],
        errors: [],
        progress: 0,
        loading: false,
        message: '',
        cin: null,
        request_id: null,
        uploading_files: false
    },
    methods: {
        start_function: function () {
            if (this.data.district == null){
                return alert(document.getElementById('myText').innerHTML);
            }
            this.startRequest = true;
            this.load_deputes(this.data.district);
        },
        getLocal: function () {
            return document.getElementById('Local').innerHTML
        },
        load_deputes: function () {
            axios.get('/api/deputes/' + this.data.district)
                .then((response) => {
                    this.deputes = response.data;
                    if (this.deputes.length > 0)
                        this.profiles = [];
                    else
                        this.load_profiles();
                });
        },
        load_profiles: function () {
            axios.get('/api/profiles/' + this.data.district)
                .then((response) => {
                    this.profiles = response.data;
                    this.deputes = [];
                });
        },
        post_request: function () {
            this.loading = true;
            this.data.recaptcha = document.getElementById('recaptcha').value;
            axios.post('/' + this.getLocal() + '/store_request', this.data)
                .then((response) => {
                    this.resetCaptcha();
                    this.message = response.data.message;
                    this.request_id = response.data.request_id;
					this.cle = response.data.request_key;
                    this.validationErrors = '';
                    if(this.file_to_upload.length > 0) {
                        this.uploading_files = true
                        for (var i = 0; i < this.file_to_upload.length; i++) {
                            this.errors = [];
                            const fa = new FormData();
                            fa.append('file', this.file_to_upload[i]);
                            axios.post('/api/add_file/' + this.request_id+'|'+this.cle, fa, {
                                onUploadProgress: uploadEvent => {
                                    this.progress = Math.round(uploadEvent.loaded / uploadEvent.total * 100);
                                }
                            })
                                .then((res) => {
                                    console.log('new file add');
                                })
                                .catch(error => {
                                    console.log(error.response.data.errors);
                                });
                        }
                        this.uploading_files = false
                    } else {
                        this.uploading_files = false
                    }
                    this.loading = false;
                })
                .catch(error => {
                    this.resetCaptcha();
                    if (error.response.status == 422) {
                        this.validationErrors = error.response.data.errors;
                    }
                    this.loading = false;
                });
        },
        getPayload(e){
            this.data.phone = e
        },
        onFileChange(event){
            this.file_to_upload = event.target.files;
        },
        async resetCaptcha() {
            await window.grecaptcha.execute('6LckeOEUAAAAAO8JrS6O00FuIR3Gw0OCq6zfjP3b', {action: 'contact'})
                .then(function (token) {
                    document.getElementById('recaptcha').value = token;
                });
        }
    }
});
