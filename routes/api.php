<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get(
    '/user',
    function (Request $request) {
        return $request->user();
    }
);

Route::group(
    [
        'namespace' => 'Api',
    ],
    function () {
        Route::post('post_request', 'AjaxController@post_request');
        Route::get('districts', 'AjaxController@districts');
        Route::get('district/{id}', 'AjaxController@get_district');
        Route::get('deputes/{district_id}', 'AjaxController@deputes');
        Route::get('profiles/{district_id}', 'AjaxController@profiles');
        Route::get('admins/{district_id}', 'AjaxController@admins');
        Route::post('request', 'AjaxController@request');
        Route::post('comments', 'AjaxController@comments');
        Route::post('post_comment', 'AjaxController@store_comment');
        Route::post('files', 'AjaxController@files');
        Route::post('add_file/{request_id}', 'AjaxController@add_file');
        Route::post('remove_file', 'AjaxController@remove_file');
        Route::get('gov_requests_list', 'AjaxController@public_gov_requests');
    }
);


Route::group(
    [
        'prefix' => 'admin',
        'namespace' => 'Admin',
        'middleware' => 'auth',
    ],
    function () {
        Route::get('users_list', 'DatatableController@users_list');
        Route::get('profiles_list', 'DatatableController@profiles_list');
        Route::get('admin_users_list', 'DatatableController@admin_users_list');
        Route::get('category_list', 'DatatableController@category_list');
        Route::get('district_list', 'DatatableController@district_list');
        Route::get('status_list', 'DatatableController@status_list');
        Route::get('ministry_list', 'DatatableController@ministry_list');
        Route::get('team_list', 'DatatableController@team_list');
        Route::get('gov_requests_list', 'DatatableController@gov_requests');
        Route::get('users_types_list', 'DatatableController@users_types_list');
    }
);
