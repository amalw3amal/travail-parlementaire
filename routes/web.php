<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('auth');

Route::group(
    [
        'namespace' => 'Admin',
        'prefix' => 'admin',
        'middleware' => 'auth'
    ],
    function () {
        Route::get('/', 'AdminController@dashboard')->name('dashboard');
        Route::get('request', 'RequestController@requests')->name('requests');
        Route::get('request/{request_id}', 'RequestController@show_request')->name('show_admin_request');
        Route::post('request/', 'RequestController@update_request')->name('update_request');
        Route::post('update_applicant', 'RequestController@update_applicant')->name('update_applicant');
		Route::post('update_request_prive', 'RequestController@update_request_prive')->name('update_request_prive');
		
		//requestbackend
		Route::get('request_create', 'RequestController@create')->name('request_create');
		Route::post('request_save', 'RequestController@request_save')->name('request_save');
		
		

        Route::group(
            [
                'middlerware' => 'can:applicant'
            ],
            function () {
                Route::get('users', 'AdminController@users')
                    ->name('applicant')
                    ->middleware('can:applicant.is_visible');
                Route::get('user/{user_id}', 'AdminController@user')
                    ->name('show_applicant')
                    ->middleware('can:applicant.is_read');
            }
        );

        Route::group(
            [
                'middleware' => 'can:gov_request',
            ],
            function () {
                Route::get('gov_request', 'GovReqController@index')
                    ->name('gov_request')
                    ->middleware('can:gov_request.is_visible');
                Route::get('gov_request/create', 'GovReqController@create')
                    ->name('create_gov_request')
                    ->middleware('can:gov_request.is_create');
                Route::post('gov_request', 'GovReqController@store')
                    ->name('store_gov_request')
                    ->middleware('can:gov_request.is_create');
                Route::get('gov_request/{req_id}', 'GovReqController@show')
                    ->name('show_gov_request')
                    ->middleware('can:gov_request.is_read');
                Route::get('gov_request/{req_id}/edit', 'GovReqController@edit')
                    ->name('edit_gov_request')
                    ->middleware('can:gov_request.is_edit');
                Route::post('gov_request/{req_id}/update', 'GovReqController@update')
                    ->name('update_gov_request')
                    ->middleware('can:gov_request.is_edit');
                Route::post('gov_request/{req_id}', 'GovReqController@post_comment')
                    ->name('gov_request_post_comment')
                    ->middleware('can:gov_request.is_edit');
            }
        );

        Route::group(
            [
                'middleware' => 'can:user_management',
            ],
            function () {
                Route::get('users_admin', 'AdminController@users_admin')
                    ->name('admin_users')
                    ->middleware('can:user_management.is_visible');
                Route::get('users_admin/create', 'AdminController@create_users_admin')
                    ->name('create_admin_user')
                    ->middleware('can:user_management.is_create');
                Route::post('users_admin', 'AdminController@store_new_users')
                    ->name('store_new_user')
                    ->middleware('can:user_management.is_create');
                Route::get('edit_users_admin/{user_id}', 'AdminController@edit_users_admin')
                    ->name('edit_user_admin')
                    ->middleware('can:user_management.is_edit');
                Route::post('edit_users_admin/{user_id}', 'AdminController@update_users')
                    ->name('update_user')
                    ->middleware('can:user_management.is_edit');
                Route::get('users_admin/{user_id}', 'AdminController@remove_user')
                    ->name('remove_user')
                    ->middleware('can:user_management.is_delete');
                Route::get('profiles', 'ProfileController@profiles')
                    ->name('profiles')
                    ->middleware('can:user_management.is_visible');
                Route::get('create_profile', 'ProfileController@create_profile')
                    ->name('create_profile')
                    ->middleware('can:user_management.is_create');
                Route::post('profile', 'ProfileController@store_new_profile')
                    ->name('store_new_profile')
                    ->middleware('can:user_management.is_create');
                Route::get('profile/{profile_id}/edit', 'ProfileController@edit_profile')
                    ->name('edit_profile')
                    ->middleware('can:user_management.is_edit');
                Route::get('profile/{profile_id}/remove_avatar', 'ProfileController@remove_avatar')
                    ->name('remove_profile_avatar')
                    ->middleware('can:user_management.is_edit');
                Route::post('profile/{profile_id}/edit', 'ProfileController@update_profile')
                    ->name('update_profile')
                    ->middleware('can:user_management.is_edit');
                Route::get('profile/{profile_id}/remove', 'ProfileController@remove')
                    ->name('remove_profile')
                    ->middleware('can:user_management.is_delete');
            }
        );

        //User Type
        Route::group(
            [
                'middleware' => 'can:roles',
            ],
            function () {
                Route::get('settings/user_types', 'UserTypeController@index')
                    ->name('user_types')
                    ->middleware('can:roles.is_visible');
                Route::get('settings/user_type/create', 'UserTypeController@create')
                    ->name('create_user_type')
                    ->middleware('can:roles.is_create');
                Route::post('settings/user_type', 'UserTypeController@store')
                    ->name('store_user_type')
                    ->middleware('can:roles.is_create');
                Route::get('settings/user_type/{role_id}', 'UserTypeController@show')
                    ->name('show_user_type')
                    ->middleware('can:roles.is_read');
                Route::get('settings/user_type/{role_id}/edit', 'UserTypeController@edit')
                    ->name('edit_user_type')
                    ->middleware('can:roles.is_edit');
                Route::post('settings/user_type/{role_id}/edit', 'UserTypeController@update')
                    ->name('update_user_type')
                    ->middleware('can:roles.is_edit');
                Route::get('settings/user_type/{role_id}/remove', 'UserTypeController@remove')
                    ->name('remove_user_type')
                    ->middleware('can:roles.is_delete');
            }
        );

        Route::group(
            [
                'prefix' => 'settings',
                'middleware' => 'can:settings',
            ],
            function () {
                // Districts
                Route::get('districts', 'DistrictController@index')
                    ->name('districts')
                    ->middleware('can:settings.is_visible');
                Route::get('district/create', 'DistrictController@create')
                    ->name('create_district')
                    ->middleware('can:settings.is_create');
                Route::post('district', 'DistrictController@store')
                    ->name('store_district')
                    ->middleware('can:settings.is_create');
                Route::get('district/{district_id}/remove', 'DistrictController@remove')
                    ->name('remove_district')
                    ->middleware('can:settings.is_delete');
                    Route::get('district/{district_id}/edit', 'DistrictController@edit_district')
                    ->name('edit_district');
                Route::post('district/{district_id}/update', 'DistrictController@update_district')
                    ->name('update_district');
                // Categories
                Route::get('categories', 'CategoryController@index')
                    ->name('categories')
                    ->middleware('can:settings.is_visible');
                Route::get('category/create', 'CategoryController@create')
                    ->name('create_category')
                    ->middleware('can:settings.is_create');
                Route::post('category', 'CategoryController@store')
                    ->name('store_category')
                    ->middleware('can:settings.is_create');
                Route::get('category/{category_id}/remove', 'CategoryController@remove')
                    ->name('remove_category')
                    ->middleware('can:settings.is_delete');
                Route::get('category/{category_id}/edit', 'CategoryController@edit_category')
                    ->name('edit_category');
                Route::post('category/{category_id}/update', 'CategoryController@Update_Category')
                    ->name('update_category');
                //Status
                Route::get('status', 'StatusController@index')
                    ->name('status')
                    ->middleware('can:settings.is_visible');
                Route::get('status/create', 'StatusController@create')
                    ->name('create_status')
                    ->middleware('can:settings.is_create');
                Route::post('status', 'StatusController@store')
                    ->name('store_status')
                    ->middleware('can:settings.is_create');
                Route::get('status/{status_id}/remove', 'StatusController@remove')
                    ->name('remove_status')
                    ->middleware('can:settings.is_delete');
                Route::get('status/{status_id}/edit', 'StatusController@edit_status')
                    ->name('edit_status');
                Route::post('status/{status_id}/update', 'StatusController@update_status')
                        ->name('update_status');
               
                //Gov
                Route::get('ministry', 'MinistryController@index')
                    ->name('ministry')
                    ->middleware('can:settings.is_visible');
                Route::get('ministry/create', 'MinistryController@create')
                    ->name('create_ministry')
                    ->middleware('can:settings.is_create');
                Route::post('ministry', 'MinistryController@store')
                    ->name('store_ministry')
                    ->middleware('can:settings.is_create');
                Route::get('ministry/{ministry_id}/remove', 'MinistryController@remove')
                    ->name('remove_ministry')
                    ->middleware('can:settings.is_delete');
                    Route::get('ministry/{ministry_id}/edit', 'MinistryController@edit_ministry')
                    ->name('edit_ministry');
                Route::post('ministry/{ministry_id}/update', 'MinistryController@Update_ministry')
                    ->name('update_ministry');
                //team
                Route::get('team', 'TeamController@index')
                    ->name('team')
                    ->middleware('can:settings.is_visible');
                Route::get('team/create', 'TeamController@create')
                    ->name('create_team')
                    ->middleware('can:settings.is_create');
                Route::post('team', 'TeamController@store')
                    ->name('store_team')
                    ->middleware('can:settings.is_create');
                Route::get('team/{team_id}/remove', 'TeamController@remove')
                    ->name('remove_team')
                    ->middleware('can:settings.is_delete');
                Route::get('team/{team_id}/edit', 'teamController@edit_team')
                    ->name('edit_team');
                Route::post('team/{team_id}/update', 'teamController@Update_team')
                    ->name('update_team');

            }
        );
    }
);

Route::get('/api/admin/requests_list', 'Admin\DatatableController@requests_list');
Route::get('/api/admin/requests_list/{user_id}', 'Admin\DatatableController@requests_list_applicant');

Route::get(
    '/',
    function () {
        return redirect()->to('ar');
    }
);

Route::group(
    [
        'namespace' => 'Front',
    ],
    function () {
        Route::group(
            [
                'prefix' => '{lang}',
                'middleware' => 'language',
            ],
            function () {
                Route::get('/', 'PageController@index')->name('home');
                Route::get('/about', 'PageController@about')->name('about');
                Route::get('/confidentiality', 'PageController@confidentiality')->name('confidentiality');
                Route::match(['get', 'post'], 'request', 'RequestController@show_request')->name('show_request');
                Route::get('gov_requests', 'RequestController@public_gov_requests')->name('public_gov_requests');
                Route::post('store_request', 'RequestController@store_request')->name('store_new_request');
            }
        );
    }
);
