<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Support\Facades\Crypt;
use ESolution\DBEncryption\Traits\EncryptedAttribute;
use DB;

class Request extends Model
{
use EncryptedAttribute;
	protected $fillable = [
        'key',
        'applicant_id',
        'category_id',
        'district_id',
        'user_id',
        'team_id',
        'status_id',
		'conv_status_id',
        'title',
        'description',
        'request_information',
        'closed',
        'proof_file'
    ];
	protected $encryptable  = [ 'title','description'];
	
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id', 'id');
    }

    public function applicant()
    {
        return $this->belongsTo(Applicant::class, 'applicant_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'request_id', 'id');
    }
	
	 public function team()
    {
        return $this->belongsTo(Team::class, 'team_id', 'id');
    }

    public function gov_reqs()
    {
        return $this->belongsToMany(
            GovRequest::class,
            'gov_requests_requests',
            'request_id',
            'gov_request_id',
            'id',
            'id'
        );
    }
	
	public function conv_status()
    {
        return $this->belongsTo(ConversationStatus::class, 'conv_status_id', 'id');
    }

    public function setTitle($value)
    {
        $this->attributes['title'] = $this->setAttribute($value);
    }

    public function setDescription($value)
    {
        $this->attributes['description'] = $this->setAttribute($value);
    }

    public function getTitle()
    {
        return $this->getAttribute('title');
    }

    public function getDescription()
    {
        return $this->getAttribute('description');
    }

}
