<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConversationStatus extends Model
{
protected $fillable = ['name', 'active'];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
