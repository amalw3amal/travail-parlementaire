<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class GovRequest extends Model
{
    protected $table = 'gov_requests';
    protected $fillable = [
        'id',
        'user_id',
        'request_unique_number',
        'title',
        'description',
        'file',
        'file_name',
        'category_id',
        'ministry_id',
        'district_id',
        'request_type',
        'created_on',
        'sent_on',
        'response_on'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function ministry()
    {
        return $this->belongsTo(Ministry::class, 'ministry_id', 'id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany(GovRequestComment::class, 'gov_request_id', 'id');
    }

    public function reqs()
    {
        return $this->belongsToMany(
            Request::class,
            'gov_requests_requests',
            'gov_request_id',
            'request_id',
            'id',
            'id'
        );
    }

    public function scopeFilter($query, $filters)
    {
        if (isset($filters['title'])) {
            if ($title = $filters['title']) {
                if ($title != '') {
                    $query->where('title', 'Like', '%' . $title . '%');
                }
            }
        }
    }
}
