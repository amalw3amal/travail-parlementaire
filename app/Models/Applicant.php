<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Support\Facades\Crypt;
use ESolution\DBEncryption\Traits\EncryptedAttribute;

class Applicant extends Model
{
use EncryptedAttribute;
    protected $encryptable  = [ 'cin','name', 'gender', 'code_number', 'phone_number', 'email', 'user_id'];
	protected $fillable = ['cin', 'name', 'gender', 'code_number', 'phone_number', 'email', 'user_id'];
	

    public function requests()
    {
        return $this->hasMany(Request::class, 'applicant_id', 'id');
    }
	
	 public function setCin($value)
    {
        $this->attributes['cin'] = $this->setAttribute($value);
    }
	
	public function getCin()
    {
		
        return $this->getAttribute('cin');
    }

    public function setName($value)
    {
        $this->attributes['name'] = $this->setAttribute($value);
    }

    public function getName()
    {
        return $this->getAttribute('name');
    }

    public function setGender($value)
    {
        $this->attributes['gender'] = $this->setAttribute($value);
    }

    public function getGender()
    {
        return $this->getAttribute('gender');
    }

    public function setEmail($value)
    {
        $this->attributes['email'] = $this->setAttribute($value);
    }

    public function getEmail()
    {
        return $this->getAttribute('email');
    }

    public function setPhoneNumber($value)
    {
        $this->attributes['phone_number'] = $this->setAttribute($value);
    }

    public function getPhoneNumber()
    {
        return $this->getAttribute('phone_number');
    }

    public function scopeFilter($query, $filters)
    {
        if (isset($filters['cin'])) {
            if ($cin = $filters['cin']) {
                if ($cin != null && strlen($cin) > 0) {
                    $query->whereEncrypted('cin', $cin);
                }
            }
        }
        if (isset($filters['email'])) {
            if ($email = $filters['email']) {
                if ($email != '') {
                    $query->whereEncrypted('email', 'Like', '%' . $email . '%');
                }
            }
        }
        if (isset($filters['name'])) {
            if ($name = $filters['name']) {
                if ($name != '') {
                    $query->whereEncrypted('name','Like', '%' . $name . '%');
                }
            }
        }
    }
}
