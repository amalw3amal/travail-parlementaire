<?php

namespace App\Http\Controllers\Front;

use App\Mail\TrakApplicantCode;
use App\Models\Applicant;
use App\Models\Category;
use App\Models\District;
use App\Models\File;
use App\Models\Profile;
use App\Models\Status;
use App\Rules\EmailVerification;
use App\Rules\PhoneValidation;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Ixudra\Curl\Facades\Curl;
use Mockery\Exception;

class RequestController extends Controller
{
    public function create_request(Request $request)
    {
        if ($district = District::where('id', $request->district_id)->first()) {
            $deputes = User::where('district_id', $district->id)
                ->where('role_id', env('DEPUTE_ID', 0))
                ->get();
            if ($deputes->count() == 0) {
                $profiles = Profile::where('district_id', $district->id)
                    ->get();
            }
        }

        return view('pages.request.create', compact('district', 'deputes', 'profiles'));
    }

    public function show_request(Request $request)
    {
        if ($request->password && $request->cin) {
            $user_request = \App\Models\Request::where('key', $request->password)
                ->with('applicant')
                ->firstOrFail();
            if ($user_request->applicant->cin != $request->cin) {
                abort(404);
            }
            $user_status = null;
            if ($user_request->status != null) {
                if ($user_request->status->parent_status != null) {
                    $user_status = Status::where('id', $user_request->status->parent_status)->first();
                } else {
                    $user_status = $user_request->status;
                }
            }
        } else {
            return view('pages.request.show_login_request');
        }

        return view('pages.request.show', compact('user_request', 'user_status'));
    }

    public function store_request(Request $request)
    {
        if (config('app.active_recaptcha') === true) {
            $response = Curl::to('https://www.google.com/recaptcha/api/siteverify')
                ->withData(
                    [
                        'secret' => env('SECRET_KEY'),
                        'response' => $request['recaptcha']
                    ]
                )
                ->post();

            if (json_decode($response)->success != true) {
                throw new Exception('403');
            }
        }

        $this->validate(
            $request,
            [
                'cin' => 'required|min:8|max:8|alpha-num',
                'name' => 'required|min:4',
                'gender' => 'required',
                'phone_number' => new PhoneValidation($request->phone),
                'email' => new EmailVerification(),
                'category' => 'required',
                'title' => 'required|min:4',
                'description' => 'required|min:10',
                'district' => 'required'
            ]
        );

        $applicant = Applicant::whereEncrypted('cin', $request->cin)
            ->first();
			
		
		if($applicant)
		{
			$exist_request = \App\Models\Request::where('category_id', $request->category)
			->where('applicant_id', $applicant->id)
			->whereEncrypted('title', $request->title)
			->where('status_id','!=',8)
			->first();
		

		  
				if($exist_request) 
				{
					//$Count = $exist_request->count();
				
					
					return response()->json(
						[
							'request_id' => $exist_request->id,
							'applicant_cin' => $applicant->cin,
							'request_key' => $exist_request->key,
							'message' => Lang::get('layout.exist_message', ['cin' => $applicant->cin, 'key' => $exist_request->key]),
						]
					);
					
					exit;
					
				
				}
				else{
					
					
					
					if($applicant->email != $request->email 
					OR $applicant->phone_number != '00' . $request->phone['country']['dialCode'] . $request->phone_number)
					{
					$applicant = Applicant::firstOrCreate(
						['cin' => $request->cin]
					);
					$applicant->email = $request->email;
					$applicant->name = $request->name;
					$applicant->gender = $request->gender;
					$applicant->phone_number = '00' . $request->phone['country']['dialCode'] . $request->phone_number;
					$applicant->save();
					}

					do {
						$new_key = strtoupper(str_random(5));
						$find_key = \App\Models\Request::where('key', $new_key)->first();
					} while ($find_key != null);

					$req = \App\Models\Request::create(
					[
						'key' => $new_key,
						'applicant_id' => $applicant->id,
						'category_id' => $request->category,
						'district_id' => $request->district,
						'status_id' => 1,
						'title' => $request->title,
						'description' => $request->description
					]
					);

					if (config('app.sendmail') == true) {
					$this->dispatch(
						new \App\Jobs\TrakApplicantCode(
							$req->title,
							$applicant->name,
							$applicant->email,
							$applicant->cin,
							$new_key,
							$req->created_at
						)
					);
					}

					if (config('app.send_sms') == true
						&& $request->phone['regionCode'] == 'FR'
					) {
						Curl::to(config('app.api_sms_uri'))
							->withHeaders(
								[
									'x-access-token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzYXNzaSIsImlhdCI6MTU3NzAyOTA5MSwiZXhwIjoxNjA4NTY1MTA0LCJhdWQiOiJzbXMtc2VydmVyMDEuZGRucy5uZXQiLCJzdWIiOiJzbXNAc21zLXNlcnZlcjAxLmRkbnMubmV0In0.hbqDAyb946sMbdCQJ5FVQvpqtuQGCBlCJp2L2x3ECag',
									'Content-Type' => 'application/json'
								]
							)
							->withData(
								[
									'number' => str_replace(' ', '', $request->phone_number),
									'message' => trans('layout.sms_message', ['cin' => $applicant->cin, 'other' => $new_key]),
									'validity' => 'DAY',
								]
							)
							->asJson()
							->post();
					}

					return response()->json(
						[
							'request_id' => $req->id,
							'applicant_cin' => $applicant->cin,
							'request_key' => $req->key,
							'message' => Lang::get('layout.registered_message', ['cin' => $applicant->cin, 'key' => $req->key]),
						]
					);
				}
		
		
			
		}
        else {
            $applicant = Applicant::firstOrCreate(
                ['cin' => $request->cin]
            );
            $applicant->email = $request->email;
            $applicant->name = $request->name;
            $applicant->gender = $request->gender;
            $applicant->phone_number = '00' . $request->phone['country']['dialCode'] . $request->phone_number;
            $applicant->save();
        

			do {
				$new_key = strtoupper(str_random(5));
				$find_key = \App\Models\Request::where('key', $new_key)->first();
			} while ($find_key != null);

			$req = \App\Models\Request::create(
            [
                'key' => $new_key,
                'applicant_id' => $applicant->id,
                'category_id' => $request->category,
                'district_id' => $request->district,
                'status_id' => 1,
                'title' => $request->title,
                'description' => $request->description
            ]
			);

			if (config('app.sendmail') == true) {
            $this->dispatch(
                new \App\Jobs\TrakApplicantCode(
                    $req->title,
                    $applicant->name,
                    $applicant->email,
                    $applicant->cin,
                    $new_key,
                    $req->created_at
                )
            );
			}

			if (config('app.send_sms') == true
				&& $request->phone['regionCode'] == 'FR'
			) {
				Curl::to(config('app.api_sms_uri'))
					->withHeaders(
						[
							'x-access-token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzYXNzaSIsImlhdCI6MTU3NzAyOTA5MSwiZXhwIjoxNjA4NTY1MTA0LCJhdWQiOiJzbXMtc2VydmVyMDEuZGRucy5uZXQiLCJzdWIiOiJzbXNAc21zLXNlcnZlcjAxLmRkbnMubmV0In0.hbqDAyb946sMbdCQJ5FVQvpqtuQGCBlCJp2L2x3ECag',
							'Content-Type' => 'application/json'
						]
					)
					->withData(
						[
							'number' => str_replace(' ', '', $request->phone_number),
							'message' => trans('layout.sms_message', ['cin' => $applicant->cin, 'other' => $new_key]),
							'validity' => 'DAY',
						]
					)
					->asJson()
					->post();
			}

			return response()->json(
				[
					'request_id' => $req->id,
					'applicant_cin' => $applicant->cin,
					'request_key' => $req->key,
					'message' => Lang::get('layout.registered_message', ['cin' => $applicant->cin, 'key' => $req->key]),
				]
			);
		}
    
	}

    public function store_proof_file($request_id, Request $request)
    {
        $req = \App\Models\Request::where('id', $request_id)->firstOrFail();
        if ($proof_file = $request->file('proof_file')) {
            $ex = $proof_file->getClientOriginalExtension();
            $new_name = md5(str_random(20) . time()) . '.' . $ex;
            $proof_file->move(public_path('uploads'), $new_name);
            $req->proof_file = 'uploads/' . $new_name;
            $req->save();
        }
    }

    public function store_request_files($request_id, Request $request)
    {
        $req = \App\Models\Request::where('id', $request_id)->firstOrFail();
        if ($files = $request->file('files')) {
            foreach ($files as $file) {
                $ex = $file->getClientOriginalExtension();
                $new_name = md5(str_random(20) . time()) . '.' . $ex;
                $file->move(public_path('uploads'), $new_name);
                File::create(
                    [
                        'name' => str_replace($ex, '', '.' . $file->getClientOriginalName()),
                        'src' => 'uploads/' . $new_name,
                        'request_id' => $req->id
                    ]
                );
            }
        }
    }

    public function public_gov_requests()
    {
        return view('pages.gov_requests');
    }
}
