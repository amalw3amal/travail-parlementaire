<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class PageController extends Controller
{

    public function index()
    {
        if (config('app.requested_prof') == true) {
            $districts = \App\Models\District::Active()
                ->whereHas(
                    'users',
                    function ($query) {
                        $query->where('role_id', env('DEPUTE_ID', 2));
                        $query->orWhere('role_id', env('OTHER_DEPUTE_ID', 1));
                    }
                )
                ->orWhereHas('profiles')
                ->get();
        } else {
            $districts = \App\Models\District::Active()
                ->get();
        }

        $categories = \App\Models\Category::Active()->get();

        return view('pages.welcome', compact('districts', 'categories'));
    }

    public function about()
    {
        return view('pages.about');
    }

    public function confidentiality()
    {
        return view('pages.confidentiality');
    }

}
