<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\District;
use App\Models\GovRequest;
use App\Models\GovRequestComment;
use App\Models\Ministry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GovReqController extends Controller
{

    public function index(Request $request)
    {
        $reqs = $request->all();
        $categories = Category::Active()->get();
        $ministries = Ministry::Active()->get();

        return view('admin.gov_request.index', compact('categories', 'ministries', 'reqs'));
    }

    public function create()
    {
        $categories = Category::Active()->get();
        $ministries = Ministry::Active()->get();
        $requests = \App\Models\Request::all();
        $districts = District::Active()->get();

        return view('admin.gov_request.create', compact('categories', 'ministries', 'requests', 'districts'));
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'request_type' => 'required',
                'title' => 'required',
                'description' => 'required',
                'requests' => 'required',
                'district' => 'required',
                'category' => 'required',
                'ministry' => 'required',
                'file' => 'required'
            ]
        );

        if ($file = $request->file('file')) {
            $ex = $file->getClientOriginalExtension();
            $file_name = $file->getClientOriginalName();
            $new_name = md5(str_random(20) . time()) . '.' . $ex;
            $file->move(public_path('uploads/'), $new_name);
        }

        $gov_req = GovRequest::create(
            [
                'request_unique_number' => $request->request_unique_number.'/'.$request->annee,
                'request_type' => $request->request_type,
                'user_id' => auth()->id(),
                'title' => $request->title,
                'description' => $request->description,
                'file' => 'uploads/' . $new_name,
                'file_name' => $file_name,
                'district_id' => $request->district ? $request->district : null,
                'category_id' => $request->category ? $request->category : null,
                'ministry_id' => $request->ministry ? $request->ministry : null,
                'created_on' => $request->created_on ? $request->created_on : null,
                'sent_on' => $request->sent_on ? $request->sent_on : null,
                'response_on' => $request->response_on ? $request->response_on : null,
            ]
        );

        if ($request->requests) {
            if (is_array($request->requests)) {
                foreach ($request->requests as $req) {
                    $gov_req->reqs()->attach($req);
                }
            } else {
                $gov_req->reqs()->attach($request->requests);
            }
        }

        session()->flash('message', 'Correspendance avec le gouvernement crée avec succès!');
       /* return redirect()
            ->route('gov_request');*/
		return redirect()->back();
    }

    public function show($request_id)
    {
        $req = GovRequest::where('id', $request_id)
            ->with('reqs')
            ->firstOrFail();
        return view('admin.gov_request.show', compact('req'));
    }

    public function edit($request_id)
    {
        $req = GovRequest::where('id', $request_id)
            ->with('reqs')
            ->firstOrFail();

        $categories = Category::Active()->get();
        $ministries = Ministry::Active()->get();
        $requests = \App\Models\Request::all();
        $districts = District::Active()->get();

        return view('admin.gov_request.edit', compact('req', 'categories', 'ministries', 'requests', 'districts'));
    }

    public function update($request_id, Request $request) {

        $gov_req = GovRequest::where('id', $request_id)
            ->with('reqs')
            ->firstOrFail();

        $this->validate(
            $request,
            [
                'request_type' => 'required',
                'title' => 'required',
                'description' => 'required',
                'requests' => 'required',
                'district' => 'required',
                'category' => 'required',
                'ministry' => 'required'
            ]
        );

        if ($file = $request->file('file')) {
            $ex = $file->getClientOriginalExtension();
            $file_name = $file->getClientOriginalName();
            $new_name = md5(str_random(20) . time()) . '.' . $ex;
            $file->move(public_path('uploads/'), $new_name);

            GovRequest::where('id', $request_id)->update(
                [
                    'file' => 'uploads/' . $new_name,
                    'file_name' => $file_name,
                ]
            );
        }

        GovRequest::where('id', $request_id)->update(
            [
                //'request_unique_number' => $request->request_unique_number,
                'request_type' => $request->request_type,
                'user_id' => auth()->id(),
                'title' => $request->title,
                'description' => $request->description,
                'district_id' => $request->district ? $request->district : null,
                'category_id' => $request->category ? $request->category : null,
                'ministry_id' => $request->ministry ? $request->ministry : null,
                'created_on' => $request->created_on ? $request->created_on : null,
                'sent_on' => $request->sent_on ? $request->sent_on : null,
                'response_on' => $request->response_on ? $request->response_on : null,
            ]
        );

        foreach ($gov_req->reqs as $r) {
            $gov_req->reqs()->detach($r);
        }

        if ($request->requests) {
            if (is_array($request->requests)) {
                foreach ($request->requests as $req) {
                    $gov_req->reqs()->attach($req);
                }
            } else {
                $gov_req->reqs()->attach($request->requests);
            }
        }

        session()->flash('message', 'Correspendance avec le gouvernement modifiée avec succès!');
        return redirect()
            ->route('gov_request');

    }

    public function post_comment($gov_req_id, Request $request)
    {
        $req = GovRequest::where('id', $gov_req_id)->firstOrFail();

        $this->validate(
            $request,
            [
                'body' => 'required'
            ]
        );

        if ($file = $request->file('file')) {
            $ex = $file->getClientOriginalExtension();
            $file_name = $file->getClientOriginalName();
            $new_name = md5(str_random(20) . time()) . '.' . $ex;
            $file->move(public_path('uploads/'), $new_name);
        }

        GovRequestComment::create(
            [
                'gov_request_id' => $req->id,
                'created_on' => $request->created_on,
                'user_id' => auth()->id(),
                'body' => $request->body,
                'file' => isset($new_name) ? 'uploads/' . $new_name : null,
                'file_name' => isset($file_name) ? $file_name : null
            ]
        );

        session()->flash('message', 'Commentaire bien ajouté!');
        return redirect()->back();
    }
}
