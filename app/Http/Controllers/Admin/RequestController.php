<?php

namespace App\Http\Controllers\Admin;

use App\Mail\NotifApplicant;
use App\Models\Applicant;
use App\Models\Category;
use App\Models\Comment;
use App\Models\District;
use App\Models\Ministry;
use App\Models\Status;
use App\Models\Team;
use App\Models\UserType;
use App\Rules\EmailVerification;
use App\Rules\PhoneValidation;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Models\ConversationStatus;

class RequestController extends Controller
{
    public function requests(Request $request)
    {
        $reqs = $request->all();
        $status = Status::Active()->get();
        $districts = District::Active()->get();
        $users = User::all();
		$teams = Team::all();
        $applicants = Applicant::whereHas('requests')->get();
		$conv_status = ConversationStatus::Active()->get();

        return view('admin.request.index', compact('districts', 'status', 'users', 'reqs', 'applicants', 'teams','conv_status'));
    }

    public function show_request($request_id)
    {
        $request = \App\Models\Request::where('id', $request_id)->firstOrFail();

        $districts = District::Active()->get();
        $categories = Category::Active()->get();
        $ministries = Ministry::Active()->get();

        $permission = $permissions = json_decode(auth()->user()->role->permissions);
        if (
            (!auth()->user()->role->super_admin) &&
            (!isset($permission->all_requests->is_read)) &&
            (($request->user_id == null) && !isset($permission->new_request->is_read)) &&
            (($request->closed == 1) && !isset($permission->closed_request->is_read)) &&
            (($request->user_id != auth()->user()->id) && !isset($permission->selected_request->is_read))
        ) {
            abort(403);
        }

        $update = false;
        if (
            (auth()->user()->role->super_admin) ||
            (isset($permission->all_requests->is_edit)) ||
            (($request->user_id == null) && isset($permission->new_request->is_edit)) ||
            (($request->closed == 1) && isset($permission->closed_request->is_edit)) ||
            (($request->user_id != auth()->user()->id) && isset($permission->selected_request->is_edit))
        ) {
            $update = true;
        }

        $status = Status::Active()->get();
        $user_type = UserType::all();
        $users = User::all();
        $teams = Team::Active()->get();
		$conv_status = ConversationStatus::Active()->get();


			
	$requestsByCin = \App\Models\Request::select('requests.*')
	->leftJoin('applicants', 'requests.applicant_id', '=', 'applicants.id')
	->whereEncrypted('cin', $request->applicant->cin)
	->where('requests.id','!=', $request->id)
	->get();		


			
	$requestsByEmail = \App\Models\Request::select('requests.*')
	->leftJoin('applicants', 'requests.applicant_id', '=', 'applicants.id')
	->whereEncrypted('email', $request->applicant->email)
	->where('requests.id','!=', $request->id)
	->get();		
			

        return view(
            'admin.request.show_request',
            compact('request', 'status', 'users', 'user_type', 'permission', 'update', 'districts', 'categories', 'ministries', 'requestsByCin', 'requestsByEmail', 'teams','conv_status')
        );
    }

    public function update_request(Request $request)
    {
        $request_user = \App\Models\Request::where('id', $request->request_id)->firstOrFail();

        $permission = $permissions = json_decode(auth()->user()->role->permissions);

        if (
            (!auth()->user()->role->super_admin) &&
            (!isset($permission->all_requests->is_edit)) &&
            (($request_user->user_id == null) && !isset($permission->new_request->is_edit)) &&
            (($request_user->closed == 1) && !isset($permission->closed_request->is_edit)) &&
            (($request_user->user_id != auth()->user()->id) && !isset($permission->selected_request->is_edit))
        ) {
            abort(403);
        }

        $old_status = Status::where('id', $request_user->status_id)->first();
        $new_status = Status::where('id', $request->status)->first();

        $old_admin = User::where('id', $request_user->user_id)->first();
        $new_admin = User::where('id', $request->user)->first();

        $request_user->update([
            'district_id' => $request->district_id,
            'status_id' => $request->status,
            'user_id' => $request->user,
            'team_id' => $request->team,
            'role_id' => $request->user_type_id,
            'closed' => $request->closed == 1 ? 1 : 0,
            'title' => $request->title,
			'category_id' => $request->category_id
        ]);

        if ($new_status) {
            /*if ($new_status->parent_status == null || $request->request_information) {
                //Mail::to($request_user->applicant->email)->send(new NotifApplicant());
			    
				$this->dispatch(new \App\Jobs\NotifyUser(
					$request_user->key,
					$request_user->applicant->cin,
					$request_user->applicant->email,
					$request_user->title,
					$request_user->applicant->name
				));	
				
				
            }*/
            if ($old_status != $new_status) {
                Comment::create(
                    [
                        'request_id' => $request->request_id,
                        'user_id' => auth()->user()->id,
                        'applicant_id' => null,
                        'body' => auth()->user()->name . ' a changé le statut à ' . $new_status->name,
                        'hidden' => 1
                    ]
                );
            }
        }

        if ($new_admin != $old_admin) {
            if ($new_admin != null) {
                Comment::create([
                    'request_id' => $request->request_id,
                    'user_id' => auth()->user()->id,
                    'applicant_id' => null,
                    'body' => auth()->user()->name . ' a assigné le ticket à ' . $new_admin->name,
                    'hidden' => 1
                ]);

                $this->dispatch(new \App\Jobs\NotifyResponsible(
                    $request_user->id,
                    $new_admin->email
                ));
            }
        }

        session()->flash('message', 'La demande a été mise à jour!');
        return redirect()->back();
    }
	

    public function update_applicant(Request $request) {
        $request_user = \App\Models\Request::where('id', $request->request_id)->firstOrFail();
        $permission = $permissions = json_decode(auth()->user()->role->permissions);
        if (
            (!auth()->user()->role->super_admin) &&
            (!isset($permission->all_requests->is_edit)) &&
            (($request_user->user_id == null) && !isset($permission->new_request->is_edit)) &&
            (($request_user->closed == 1) && !isset($permission->closed_request->is_edit)) &&
            (($request_user->user_id != auth()->user()->id) && !isset($permission->selected_request->is_edit))
        ) {
            abort(403);
        }

        $this->validate(
            $request,
            [
                'cin' => 'required|min:8|max:8|alpha-num',
                'name' => 'required|min:4',
				//'phone_number' => new PhoneValidation($request->phone_number),
                'email' => new EmailVerification(),
            ]
        );
        $applicant = Applicant::where('id', $request_user->applicant_id)
            ->firstOrFail();
        $applicant->update([
            'cin' => $request->cin,
            'name' => $request->name,
			'gender' => $request->gender,
			'phone_number' => $request->phone_number,
			'email' => $request->email,
        ]);
        Comment::create([
            'request_id' => $request_user->id,
            'user_id' => auth()->user()->id,
            'applicant_id' => null,
            'body' => auth()->user()->name . ' a modifié le créateur du ticket!',
            'hidden' => 1
        ]);

        session()->flash('message', 'Applicant has been updated!');
        return redirect()->back();
    }
	
	public function create()
    {


        $districts = District::Active()->get();
		$categories = \App\Models\Category::Active()->get();


        return view('admin.request.create_request', compact('districts', 'categories'));
    }


	public function request_save(Request $request)
     {
	 $applicant = Applicant::where('id', 274)
            ->first();
	  do {
		$new_key = strtoupper(str_random(5));
		$find_key = \App\Models\Request::where('key', $new_key)->first();
		} while ($find_key != null);	
		
	 $req = \App\Models\Request::create(
					[
						'key' => $new_key,
						'applicant_id' => $applicant->id,
						'category_id' => $request->category,
						'district_id' => $request->district,
						'status_id' => 1,
						'title' => $request->title,
						'description' => $request->description
					]
					);
     $msg = 'Demande numéro '.$req->id.' bien ajoutée!, <br>  <a href="'. url('admin/request/'.$req->id.'') . '"> Cliquez ici  </a>  pour accèder à la page de gestion de cette demande';
	 session()->flash('message', $msg);
     return redirect()->back();
		
	 }	
	 
	 
	  public function update_request_prive(Request $request)
    {
        $request_user = \App\Models\Request::where('id', $request->request_id)->firstOrFail();

        $permission = $permissions = json_decode(auth()->user()->role->permissions);

        if (
            (!auth()->user()->role->super_admin) &&
            (!isset($permission->all_requests->is_edit)) &&
            (($request_user->user_id == null) && !isset($permission->new_request->is_edit)) &&
            (($request_user->closed == 1) && !isset($permission->closed_request->is_edit)) &&
            (($request_user->user_id != auth()->user()->id) && !isset($permission->selected_request->is_edit))
        ) {
            abort(403);
        }

        $old_status = Status::where('id', $request_user->status_id)->first();
        $new_status = Status::where('id', $request->status)->first();

        $old_admin = User::where('id', $request_user->user_id)->first();
        $new_admin = User::where('id', $request->user)->first();

        $request_user->update([
            'request_information' => $request->request_information ? 1 : 0,
			'conv_status_id' => $request->conv_status
        ]);

        session()->flash('message', 'Reglage Privé Changé!');
        return redirect()->back();
    }

}
