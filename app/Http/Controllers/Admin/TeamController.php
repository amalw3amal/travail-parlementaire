<?php

namespace App\Http\Controllers\Admin;

use App\Models\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    public function index()
    {
        return view('admin.super.settings.team.index');
    }

    public function create()
    {
        return view('admin.super.settings.team.create_team');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name_fr' => 'required',
                'name_ar' => 'required',
                'name_en' => 'required',
                'active' => 'required'
            ]
        );
        Team::create(
            [
                'name_ar' => $request->name_ar,
                'name_fr' => $request->name_fr,
                'name_en' => $request->name_en,
                'active' => $request->active,
            ]
        );
        session()->flash('message', 'A new Team has been created!');
        return redirect()->route('team');
    }

    public function remove($team_id)
    {
        $team= Team::where('id', $team_id)->first();
        $team->delete();
        session()->flash('message', 'A team has been removed!');
        return redirect()->route('team');
    }
  
    public function edit_team($team_id)
    {
        $team = Team::where('id', $team_id)->firstOrFail();
        return view('admin.super.settings.team.edit_team', compact('team'));
    }  
    public function update_team($team_id, Request $request)
    {
        $team  = Team::where('id', $team_id)->firstOrFail();
        $this->validate(
            $request,
            [
                'name_ar' => 'required|string|max:255',
                'name_fr' => 'required|string|max:255',
                'name_en' => 'required|string|max:255',
                'active' => 'required|max:1'
            ]
        );

        $team->name_fr = $request->name_fr;
        $team->name_en = $request->name_en;
        $team->name_ar = $request->name_ar;
        $team->active = $request->active;
        $team->save();

        session()->flash('message', 'The team has been updated!');
        return redirect()->route('categories');
    }

}
