<?php

namespace App\Http\Controllers\Admin;

use App\Models\Applicant;
use App\Models\District;
use App\Models\GovRequest;
use App\Models\Role;
use App\Models\UserType;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    public function dashboard()
    {
        $new_request_count = \App\Models\Request::where('status_id', 1)->count();
        $closed_request_count = \App\Models\Request::where('status_id', 7)->count();
       // $gov_request_without_answer = GovRequest::whereDoesntHave('comments')->count();
	   $gov_request_without_answer = \App\Models\Request::where('status_id', 6)->count();

        return view(
            'admin.dashboard',
            compact('new_request_count', 'closed_request_count', 'gov_request_without_answer')
        );
    }

    public function users(Request $request)
    {
        $req = $request->all();

        return view('admin.user.users', compact('req'));
    }

    public function user($applicant_id)
    {
        $applicant = Applicant::where('id', $applicant_id)->firstOrFail();
        return view('admin.user.show_applicant', compact('applicant'));
    }

    // Super admin
    public function users_admin()
    {
        return view('admin.super.user.users');
    }

    public function create_users_admin()
    {
        $districts = District::Active()->get();
        $user_types = Role::all();
        return view('admin.super.user.create_user', compact('districts', 'user_types'));
    }

    public function store_new_users(Request $request)
    {
        $this->validate(
            $request,
            [
                'district' => 'required',
                'role_id' => 'required',
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6',
            ]
        );
        User::create(
            [
                'district_id' => $request->district,
                'role_id' => $request->role_id,
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]
        );
        session()->flash('message', 'A new user has been created!');
        return redirect()->route('admin_users');
    }

    public function edit_users_admin($user_id)
    {
        $user = User::where('id', $user_id)->firstOrFail();
        $districts = District::Active()->get();
        $user_types = Role::all();
        return view('admin.super.user.edit_user', compact('districts', 'user_types', 'user'));
    }

    public function update_users($user_id, Request $request)
    {
        $user = User::where('id', $user_id)->firstOrFail();
        
        $this->validate(
            $request,
            [
                'district' => 'required',
                'role_id' => 'required',
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255',
                'password' => 'required|string|min:6',
            ]
        );

        $user->name = $request->name;
        $user->email = $request->email;
        $user->role_id = $request->role_id;
        $user->district_id = $request->district;
        $user->password = Hash::make($request->password);
        $user->save();

        session()->flash('message', 'A new user has been updated!');
        return redirect()->route('admin_users');
    }

    public function remove_user($user_id)
    {
        $user = User::where('id', $user_id)->firstOrFail();
        if (auth()->id() == $user->id) {
            session()->flash('message', 'you cant remove yourself!');
        } else {
            $user->delete();
            session()->flash('message', 'A user has been removed!');
        }
        return redirect()->route('admin_users');
    }
   

    public function settings()
    {
        return view('admin.super.settings.index');
    }

}
