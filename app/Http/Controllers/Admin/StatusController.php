<?php

namespace App\Http\Controllers\Admin;

use App\Models\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatusController extends Controller
{

    public function index()
    {
        return view('admin.super.settings.status.status');
    }

    public function create()
    {
        $parent_status = Status::Active()->where('parent_status', null)->get();
        return view('admin.super.settings.status.create_new_status', compact('parent_status'));
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required',
                'color' => 'required',
                'active' => 'required'
            ]
        );

        Status::create(
            [
                'name' => $request->name,
                'color' => str_replace('#', '', $request->color),
                'active' => $request->active,
                'parent_status' => $request->parent_status ? $request->parent_status : null
            ]
        );
        session()->flash('message', 'A new status has been created!');
        return redirect()->route('status');
    }

    public function remove($status_id )
    {
        $status = Status::where('id', $status_id)->first();
        $status->delete();
        session()->flash('message', 'A status has been removed!');
        return redirect()->route('status');
    }
    public function edit_status($status_id)
    {
        $status = Status::where('id', $status_id)->firstOrFail();
        $parent_status = Status::Active()->where('parent_status', null)->get();
        return view('admin.super.settings.status.edit_status', compact(['status','parent_status']));
    }  
    public function update_status($status_id ,  Request $request)
    {
            $status  = Status::where('id', $status_id)->firstOrFail();
            $this->validate(
                $request,
                [
                    'name' => 'required|string|max:255',
                    'color'=>'required|string|max:16'
                ]
            );
    
            $status->name = $request->name;
            $status->color = str_replace('#', '', $request->color);
            $status->active = $request->active;
            $status->parent_status = $request->parent_status ? $request->parent_status : null ; 
            $status->save();
    
            session()->flash('message', 'The status has been updated!');
            return redirect()->route('status');
        }

    
}
