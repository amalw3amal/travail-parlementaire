<?php

namespace App\Http\Controllers\Admin;

use App\Models\District;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DistrictController extends Controller
{
    public function index()
    {
        return view('admin.super.settings.district.index');
    }

    public function create()
    {
        return view('admin.super.settings.district.create_district');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name_ar' => 'required',
                'name_fr' => 'required',
                'name_en' => 'required',
                'active' => 'required'
            ]
        );
        District::create(
            [
                'name_ar' => $request->name_ar,
                'name_fr' => $request->name_fr,
                'name_en' => $request->name_en,
                'active' => $request->active
            ]
        );
        session()->flash('message', 'A new district has been created!');
        return redirect()->route('districts');
    }

    public function remove($district_id)
    {
        $district = District::where('id', $district_id)->first();
        $district->delete();
        session()->flash('message', 'A district has been removed!');
        return redirect()->route('districts');
    }
    public function edit_district($district_id)
    {
        $district = District::where('id', $district_id)->first();
        return view('admin.super.settings.district.edit_districts', compact('district'));
    } 
    public function update_district($district_id, Request $request)
    {
        $district = District::where('id', $district_id)->firstOrFail();
        $this->validate(
            $request,
            [
                'name_ar' => 'required|string|max:255',
                'name_fr' => 'required|string|max:255',
                'name_en' => 'required|string|max:255',
                'active' => 'required|max:1'
            ]
        );

        $district->name_fr = $request->name_fr;
        $district->name_en = $request->name_en;
        $district->name_ar = $request->name_ar;
        $district->active = $request->active;
        $district->save();

        session()->flash('message', 'The district has been updated!');
        return redirect()->route('districts');
    }


}
