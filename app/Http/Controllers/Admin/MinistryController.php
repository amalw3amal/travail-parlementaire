<?php

namespace App\Http\Controllers\Admin;

use App\Models\Ministry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MinistryController extends Controller
{
    public function index()
    {
        return view('admin.super.settings.ministry.index');
    }

    public function create()
    {
        return view('admin.super.settings.ministry.create_ministry');
    }

    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name_fr' => 'required',
                'name_ar' => 'required',
                'name_en' => 'required',
                'active' => 'required'
            ]
        );
        Ministry::create(
            [
                'name_ar' => $request->name_ar,
                'name_fr' => $request->name_fr,
                'name_en' => $request->name_en,
                'active' => $request->active,
            ]
        );
        session()->flash('message', 'A new Ministry has been created!');
        return redirect()->route('ministry');
    }

    public function remove($ministry_id)
    {
        $ministry = Ministry::where('id', $ministry_id)->first();
        $ministry->delete();
        session()->flash('message', 'A ministry has been removed!');
        return redirect()->route('ministry');
    } 
    public function edit_ministry($ministry_id)
    {
        $ministry = Ministry::where('id', $ministry_id)->firstOrFail();
        return view('admin.super.settings.ministry.edit_ministry', compact('ministry'));
    }  
    public function update_ministry($ministry_id, Request $request)
    {
        $ministry  = Ministry::where('id', $ministry_id)->firstOrFail();
        $this->validate(
            $request,
            [
                'name_ar' => 'required|string|max:255',
                'name_fr' => 'required|string|max:255',
                'name_en' => 'required|string|max:255',
                'active' => 'required|max:1'
            ]
        );

        $ministry->name_fr = $request->name_fr;
        $ministry->name_en = $request->name_en;
        $ministry->name_ar = $request->name_ar;
        $ministry->active = $request->active;
        $ministry->save();

        session()->flash('message', 'The ministry has been updated!');
        return redirect()->route('categories');
    }

}
