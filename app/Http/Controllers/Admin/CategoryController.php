<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use \Validator;

class CategoryController extends Controller
{
    public function index()
    {
        return view('admin.super.settings.category.show_categories');
    }
    public function create()
    {
        return view('admin.super.settings.category.create_category');
    }

    public function store(Request $request)
    {
        $rules =  getRulesStore();  
        $messages =  getMessages();  
        $validator =  Validator::make($request->all(),  $rules  ,  $messages);
        if($validator->fails()) {
            return redirect()->back()->with(['danger'=>'please consider to reinput your data']);
        }
        Category::create(
            [
                'name_ar' => $request->name_ar,
                'name_fr' => $request->name_fr,
                'name_en' => $request->name_en,
                'active' => $request->active
            ]
        );
        session()->flash('message', 'A new category has been created!');
        return redirect()->route('categories');
    }


    
    public function remove($category_id)
    {
        $category = Category::where('id', $category_id)->first();
        $category->delete();
        session()->flash('message', 'A category has been removed!');
        return redirect()->route('categories');
    }





    public function edit_category($category_id)
    {
        $category = Category::where('id', $category_id)->firstOrFail();
        return view('admin.super.settings.category.edit_category', compact('category'));
    } 





    public function update_category($category_id, Request $request)
    {
        $rules  =  $this->getRules(); 
        // $messages = $this->getMessages();
        $category = Category::where('id', $category_id)->firstOrFail();
        $this->validate(
            $request,
            $rules
        );
        $category->name_fr = $request->name_fr;
        $category->name_en = $request->name_en;
        $category->name_ar = $request->name_ar;
        $category->active = $request->active;
        $category->save();

        session()->flash('message', 'The Category has been updated!');
        return redirect()->route('categories');
    }



    public function getRulesStore(){
        $rules =  [
         'name_ar' => 'required',
        'name_fr' => 'required',
        'name_en' => 'required',
        'active' => 'required'
        ] ;
        return $rules ;   
    } 
    public function getRules() {
        $rules = [
                'name_ar' => 'required|string|max:255',
                'name_fr' => 'required|string|max:255|unique:categories,name_fr',
                'name_en' => 'required|string|max:255',
                'active' => 'required|max:1'
      ];
      return $rules;
    }
    public function getMessages (){
        $messages = [
        ];
            return $messages ;  
    }

}
