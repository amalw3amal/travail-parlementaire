<?php

namespace App\Http\Controllers\Admin;

use App\Models\Applicant;
use App\Models\GovRequest;
use App\Models\Profile;
use App\Models\Role;
use App\Models\Status;
use App\Models\UserType;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use DB;
use Yajra\DataTables\QueryDataTable;
use ESolution\DBEncryption\Encrypter;
use App\Models\ConversationStatus;

class DatatableController extends Controller
{

    public function admin_users_list()
    {
        $users = \App\User::select(['id', 'name', 'email', 'role_id', 'created_at', 'updated_at'])
            ->with('role');

        return DataTables::of($users)
            ->addColumn(
                'role',
                function ($user) {
                    return $user->role ? $user->role->name : '--';
                }
            )
            ->addColumn(
                'action',
                function ($user) {
                    $removed_link = route('remove_user',[$user->id]);
                    $action = '<a href="#" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a> ';
                    $action .= '<a href="' . route('edit_user_admin',[$user->id]) . '" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a> ';
                    $action .= '<a href="#"
                                class="btn btn-sm btn-danger"
                                onclick="if (confirm(\'Are you sure want to delete this item?\')) {
                                    return (location=\''.$removed_link.'\')
                                }">
                            <i class="fa fa-ban"></i> remove
                        </a>';
                    return $action;
                }
            )
            ->make(true);
    }

    public function users_list(Request $request)
    {
        $users = Applicant::filter($request->all());

        return DataTables::of($users)
            ->addColumn(
                'action',
                function ($user) {
                    $action = '<a href="' . route(
                            'show_applicant',
                            [$user->id]
                        ) . '" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a> ';
                    return $action;
                }
            )
            ->make(true);
    }

    public function profiles_list()
    {
        $profiles = Profile::select(
            ['id', 'district_id', 'name_fr', 'avatar', 'email', 'phone_number', 'active', 'created_at']
        );

        return DataTables::of($profiles)
            ->editColumn(
                'avatar',
                function ($profile) {
                    return asset($profile->avatar);
                }
            )
            ->addColumn(
                'district',
                function ($profile) {
                    $action = $profile->district->name_en;
                    return $action;
                }
            )
            ->addColumn(
                'action',
                function ($profile) {
                    $removed_link = route(
                        'remove_profile',
                        [$profile->id]
                    );
                    $action = '<a href="' . route(
                            'edit_profile',
                            [$profile->id]
                        ) . '" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</a>';
                    $action .= '<a href="#"
                                class="btn btn-sm btn-success"
                                style="margin-left: 5px;"
                                onclick="if (confirm(\'Are you sure want to delete this item?\')) {
                                    return (location=\''.$removed_link.'\')
                                }">
                            <i class="fa fa-ban"></i> Update
                        </a>';
                    return $action;
                }
            )
            ->make(true);
    }

    public function category_list()
    {
        $categories = \App\Models\Category::select(['id', 'name_ar', 'name_fr', 'name_en', 'active', 'created_at']);

        return DataTables::of($categories)
            ->addColumn(
                'action',
                function ($category) {
                    $action = '<a href="#"
                                class="btn btn-sm btn-success"
                                style="margin-left: 5px;"
                                onclick="if (confirm(\'Are you sure want to update this item?\')) {
                                    return (location=\''. route('edit_category',[$category->id]) .'\')
                                }">
                            <i class="fa fa-ban"></i> Update
                        </a>';
                    return $action;
                }
            )
            ->make(true);
    }

    public function district_list()
    {
        $districts = \App\Models\District::select(['id', 'name_en', 'name_fr', 'name_ar', 'active', 'created_at']);
        return DataTables::of($districts)
            ->addColumn(
                'action',
                function ($district) {
                    $action = '<a href="#"
                                class="btn btn-sm btn-success"
                                style="margin-left: 5px;"
                                onclick="if (confirm(\'Are you sure want to update this item?\')) {
                                    return (location=\''. route('edit_district',[$district->id]) .'\')}">
                                    <i class="fa fa-ban"></i> Update
                                </a>';
                    return $action;
                }
            )
            ->make(true);
    }

    public function status_list()
    {
        $status = \App\Models\Status::select(['id', 'parent_status', 'name', 'color', 'active', 'created_at']);
        return DataTables::of($status)
            ->addColumn(
                'parent',
                function ($statu) {
                    if ($statu->parent_status) {
                        $parent = Status::where('id', $statu->parent_status)->first()->name;
                    } else {
                        $parent = '--';
                    }
                    return $parent;
                }
            )
            ->addColumn(
                'action',
                function ($statu) {
                    $action = '<a href="#"
                                class="btn btn-sm btn-success"
                                style="margin-left: 5px;"
                                onclick="if (confirm(\'Are you sure want to delete this item?\')) {
                                    return (location=\''. route('edit_status',[$statu->id]) .'\')}">
                                    <i class="fa fa-ban"></i> Update
                                </a>';
                    return $action;
                }
            )
            ->make(true);
    }

    public function ministry_list()
    {
        $ministry = \App\Models\Ministry::select(['id', 'name_en', 'active', 'created_at']);
        return DataTables::of($ministry)
            ->addColumn(
                'action',
                function ($ministry) {
                    $action = '<a href="#"
                                class="btn btn-sm btn-success"
                                style="margin-left: 5px;"
                                onclick="if (confirm(\'Are you sure want to update this item?\')) {
                                    return (location=\''. route('edit_ministry',[$ministry->id]) .'\')}">
                                    <i class="fa fa-ban"></i> Update
                                </a>';
                    return $action;
                }
            )
            ->make(true);
    }

    public function team_list()
    {
        $team = \App\Models\Team::select(['id', 'name_en', 'active', 'created_at']);
        return DataTables::of($team)
            ->addColumn(
                'action',
                function ($team) {
                    $action = '<a href="#"
                                class="btn btn-sm btn-success"
                                style="margin-left: 5px;"
                                onclick="if (confirm(\'Are you sure want to delete this item?\')) {
                                    return (location=\''. route('edit_team',[$team->id]) .'\')}">
                                    <i class="fa fa-ban"></i> Update
                                </a>';
                    return $action;
                }
            )
            ->make(true);
    }

    public function requests_list(Request $request)
    {
        $permission = $permissions = json_decode(auth()->user()->role->permissions);

        if (
            (auth()->user()->role->super_admin == 1) ||
            (isset($permission->all_requests->is_visible))
        ) {
			$requests = \App\Models\Request::with('applicant')
			->with('status')
			->with('category')
			->with('district')
			->with('user')
			->with('conv_status')
			->select(['requests.*',\DB::raw('CASE 
WHEN MAX(comments.created_at) IS NOT NULL AND MAX(files.created_at) IS NULL THEN MAX(comments.created_at)
WHEN MAX(comments.created_at) IS  NULL AND MAX(files.created_at) IS NOT NULL THEN MAX(files.created_at)
WHEN MAX(comments.created_at) >= MAX(files.created_at)  THEN MAX(comments.created_at)
WHEN MAX(files.created_at) > MAX(comments.created_at) THEN MAX(files.created_at)
ELSE requests.updated_at
END AS mis_a_jour')])
			->leftJoin('comments', function($join)
                         {
                             $join->on('comments.request_id', '=', 'requests.id');
                             $join->whereNotNull('comments.applicant_id');
                         })
			->leftJoin('files', function($join)
                         {
                             $join->on('files.request_id', '=', 'requests.id');
                             $join->whereNotNull('files.applicant_id');
                         });
			
			

			
	

        } else {
            if (isset($permission->closed_request->is_visible)) {
                if (isset($permission->new_request->is_visible)) {
                    if (isset($permission->selected_request->is_visible)) {
                        
					$requests = \App\Models\Request::with('applicant')
					->with('status')
					->with('category')
					->with('district')
					->with('user')
					->with('conv_status')
					->select(['requests.*',\DB::raw('MAX(comments.created_at) AS mis_a_jour')])
					->leftJoin('comments', 'comments.request_id', '=', 'requests.id')
					->where('requests.user_id', auth()->user()->id)	
					->orWhere('requests.user_id', null)
                    ->orWhere('closed', 1);	
						

                    } else {
							$requests = \App\Models\Request::with('applicant')
							->with('status')
							->with('category')
							->with('district')
							->with('user')
							->with('conv_status')
							->select(['requests.*',\DB::raw('MAX(comments.created_at) AS mis_a_jour')])
							->leftJoin('comments', 'comments.request_id', '=', 'requests.id')
							->where('requests.user_id', null)	
							->orWhere('closed', 1);
                      
                    }
                } else {
                    if (isset($permission->selected_request->is_visible)) {
						$requests = \App\Models\Request::with('applicant')
						->with('status')
						->with('category')
						->with('district')
						->with('user')
						->with('conv_status')
						->select(['requests.*',\DB::raw('MAX(comments.created_at) AS mis_a_jour')])
						->leftJoin('comments', 'comments.request_id', '=', 'requests.id')
						->where('requests.user_id', auth()->user()->id)	
						->orWhere('closed', 1);
						
                    } else {
						
						$requests = \App\Models\Request::with('applicant')
						->with('status')
						->with('category')
						->with('district')
						->with('user')
						->with('conv_status')
						->select(['requests.*',\DB::raw('MAX(comments.created_at) AS mis_a_jour')])
						->leftJoin('comments', 'comments.request_id', '=', 'requests.id')
						->where('closed', 1);
						
                    }
                }
            } else {
                if (isset($permission->new_request->is_visible)) {
                    if (isset($permission->selected_request->is_visible)) {
						$requests = \App\Models\Request::with('applicant')
						->with('status')
						->with('category')
						->with('district')
						->with('user')
						->with('conv_status')
						->select(['requests.*',\DB::raw('MAX(comments.created_at) AS mis_a_jour')])
						->leftJoin('comments', 'comments.request_id', '=', 'requests.id')
						->where('requests.user_id', auth()->user()->id)
						->orWhere('requests.user_id', null)
						->where('closed', 0);
						
                    } else {
						
						$requests = \App\Models\Request::with('applicant')
						->with('status')
						->with('category')
						->with('district')
						->with('user')
						->with('conv_status')
						->select(['requests.*',\DB::raw('MAX(comments.created_at) AS mis_a_jour')])
						->leftJoin('comments', 'comments.request_id', '=', 'requests.id')
						->where('requests.user_id', null)
						->where('closed', 0);
						
                    }
                } else {
                    if (isset($permission->selected_request->is_visible)) {
						
						$requests = \App\Models\Request::with('applicant')
						->with('status')
						->with('category')
						->with('district')
						->with('user')
						->with('conv_status')
						->select(['requests.*',\DB::raw('MAX(comments.created_at) AS mis_a_jour')])
						->leftJoin('comments', 'comments.request_id', '=', 'requests.id')
						->where('requests.user_id', auth()->user()->id)
						->orWhere('closed', 0);
						
                    } else {
						
						$requests = \App\Models\Request::with('applicant')
						->with('status')
						->with('category')
						->with('district')
						->with('user')
						->with('conv_status')
						->select(['requests.*',\DB::raw('MAX(comments.created_at) AS mis_a_jour')])
						->leftJoin('comments', 'comments.request_id', '=', 'requests.id')
						->where('closed', 0);
						
                       
                    }
                }
            }
        }
		
		
		if ($request->get('closed')) {			
		    if ($closed = $request->get('closed')) {
                if ($closed == 'open' || $closed == 'closed') {
                    if ($closed == 'open') {
                        $closed = 0;
                    } else {
                        $closed = 1;
                    }
                    $requests->where('closed', $closed);
                }
            }		
			
		}
		
		if ($request->get('status')) {		
		    if ($status = $request->get('status')) {
                if ($status == 'waiting') {
                    $requests->where('status_id', null);
                }
                if ($status != '' && $status != 'waiting') {
                    $requests->where('status_id', $status);
                }
            }		
			
		}
		
		if ($request->get('district')) {	
		    if ($district = $request->get('district')) {
                 if ($district != '') {
                    $requests->where('district_id', $district);
                }
            }		
			
		}
		
		if ($request->get('user')) {	
		    if ($user = $request->get('user')) {
                 if ($user != '') {
                    $requests->where('requests.user_id', $user);
                }
            }		
			
		}
		
		if ($request->get('applicant')) {		
		    if ($applicant = $request->get('applicant')) {
                if ($applicant != '') {
                    $requests->where('requests.applicant_id', $applicant);
                }
            }		
			
		}
		
		if ($request->get('team')) {		
		    if ($team = $request->get('team')) {
                 if ($team != '') {
                    $requests->where('team_id', $team);
                }
            }		
			
		}
		
		if ($request->get('hasGovReq')) {		
		    if ($hasGovReq = $request->get('hasGovReq')) {
                if ($hasGovReq != '') {
                    if ($hasGovReq == 'yes') {
                        $requests->whereHas('gov_reqs');
                    } else {
                        $requests->whereDoesntHave('gov_reqs');
                    }
                }
            }		
			
		}
		
		if ($request->get('date')) {		
		    if ($date = $request->get('date')) {
                if ($date != '') {
                    $date = explode('~', $date);
                    $start = str_replace(" ", "", $date[0]);
                    $end = str_replace(" ", "", $date[1]);
                    $requests->where('requests.created_at', '>=', $start)->where('requests.created_at', '<=', $end);
                }
            }		
			
		}
		
		
		if ($request->get('updated')) {
		$requests->havingRaw('mis_a_jour like "'.$request->get('updated').'%"');
		}


		if ($request->get('conv_status')) {		
		    if ($conv_status = $request->get('conv_status')) {

                if ($conv_status != '') {
                    $requests->where('conv_status_id', $conv_status);
                }
            }		
			
		}		
		
		$requests->groupBy('requests.id');
				

				
		
		$datatables=DataTables::of($requests);
		
		//$datatables->orderColumn('requests.id', 'DESC');
		
        $datatables->addColumn(
                'action',
                function ($request) {
                    $action = '<a href="' . route('show_admin_request', [$request->id]) . '" class="btn btn-sm btn-primary">
                    <i class="fa fa-eye"></i> Détails</a> ';
                    return $action;
                }
            );

    
	
	return $datatables->make(true);
    }

    public function requests_list_applicant($applicant_id)
    {
        if (auth()->user()->role->super_admin == 1) {
            $requests = \App\Models\Request::where('applicant_id', $applicant_id)
                ->with('applicant')
                ->with('status')
                ->with('category')
                ->with('district')
                ->with('user');
        } else {
            $requests = \App\Models\Request::where('applicant_id', $applicant_id)
                ->where(
                    function ($query) {
                        $query->where('user_id', auth()->user()->id)->orWhereNull('user_id');
                    }
                )
                ->with('applicant')
                ->with('status')
                ->with('category')
                ->with('district')
                ->with('user');
        }

        return DataTables::of($requests)
            ->addColumn(
                'action',
                function ($request) {
                    $action = '<a href="' . route(
                            'show_admin_request',
                            [$request->id]
                        ) . '" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> view</a> ';
                    return $action;
                }
            )
            ->make(true);
    }

    public function users_types_list()
    {
        $roles = Role::select('id', 'name', 'created_at');
        return DataTables::of($roles)
            ->addColumn(
                'action',
                function ($role) {
                    if ($role->id != 1) {
                        $action = '<a href="' . route(
                                'show_user_type',
                                ['role_id' => $role->id]
                            ) . '" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a>';
                        $action .= '<a href="' . route(
                                'edit_user_type',
                                ['role_id' => $role->id]
                            ) . '" class="btn btn-sm btn-warning" style="margin-left: 5px"><i class="fa fa-edit"></i> Edit
                            </a>';
                        $action .= '<a href="#"
                                class="btn btn-sm btn-danger"
                                style="margin-left: 5px;"
                                onclick="if (confirm(\'Are you sure want to delete this item?\')) {
                                    return (location=\''. route('remove_user_type',['role_id' => $role->id]) .'\')}">
                                    <i class="fa fa-ban"></i> Remove
                                </a>';
                    } else {
                        $action = '<a href="' . route(
                                'show_user_type',
                                ['role_id' => $role->id]
                            ) . '" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View</a>';
                    }
                    return $action;
                }
            )
            ->make(true);
    }

    public function gov_requests()
    {
        $govRequest = GovRequest::select('id', 'request_unique_number', 'user_id', 'title', 'category_id', 'ministry_id', 'created_at')
            ->with('user')->with('ministry')->with('category');

        return DataTables::of($govRequest)
            ->addColumn(
                'user',
                function ($Request) {
                    return $Request->user->name;
                }
            )
            ->addColumn(
                'ministry',
                function ($Request) {
                    if ($Request->ministry) {
                        return $Request->ministry->name_en;
                    }
                    return '--';
                }
            )
            ->addColumn(
                'category',
                function ($Request) {
                    if ($Request->category) {
                        return $Request->category->name_en;
                    }
                    return '--';
                }
            )
            ->addColumn(
                'action',
                function ($Request) {
                    $action = '<a href="' . route(
                            'show_gov_request',
                            ['id' => $Request->id]
                        ) . '" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i>Voir</a>';
                    $action .= '<a style="margin-left:5px;" href="' . route(
                            'edit_gov_request',
                            ['id' => $Request->id]
                        ) . '" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i>Editer</a>';
                    return $action;
                }
            )
            ->make(true);
    }

}
