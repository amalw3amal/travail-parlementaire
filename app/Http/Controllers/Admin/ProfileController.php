<?php

namespace App\Http\Controllers\Admin;

use App\Models\District;
use App\Models\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function profiles()
    {
        return view('admin.profile.profiles');
    }

    public function create_profile()
    {
        $districts = District::Active()->get();
        return view('admin.profile.create_profile', compact('districts'));
    }

    public function edit_profile($profile_id)
    {
        $profile = Profile::where('id', $profile_id)->firstOrFail();
        $districts = District::Active()->get();

        return view('admin.profile.edit_profile', compact('profile', 'districts'));
    }

    public function remove_avatar($profile_id)
    {
        $profile = Profile::where('id', $profile_id)->firstOrFail();
        if (is_file($profile->avatar)) {
            unlink(public_path($profile->avatar));
        }
        $profile->avatar = null;
        $profile->save();
        session()->flash('message', 'Profile avatar has been removed!');
        return back();
    }

    public function update_profile($profile_id, Request $request)
    {
        $this->validate(
            $request,
            [
                'name_ar' => 'required',
                'name_fr' => 'required',
                'name_en' => 'required',
                'district' => 'required',
                'email' => 'required',
                'phone_number' => 'required'
            ]
        );
        $profile = Profile::where('id', $profile_id)->firstOrFail();
        if ($file = $request->file('avatar')) {
            $ex = $file->getClientOriginalExtension();
            //$new_name = md5(str_random(20) . time()) . '.' . $ex;
            $new_name = $file->getFilename() . '.' . $ex;
            $file->move(public_path('uploads/profiles'), $new_name);
        }
        $profile->update(
            [
                'name_ar' => $request->name_ar,
                'name_fr' => $request->name_fr,
                'name_en' => $request->name_en,
                'district' => $request->district,
                'email' => $request->email,
                'avatar' => isset($new_name) ? 'uploads/profiles/' . $new_name : $profile->avatar,
                'phone_number' => $request->phone_number,
                'active' => $request->active
            ]
        );
        session()->flash('message', 'A profile has been updated!');
        return redirect()->route('profiles');
    }

    public function store_new_profile(Request $request)
    {
        $this->validate(
            $request,
            [
                'name_ar' => 'required',
                'name_fr' => 'required',
                'name_en' => 'required',
                'district' => 'required',
                'email' => 'required',
                'phone_number' => 'required'
            ]
        );

        if ($file = $request->file('avatar')) {
            $ex = $file->getClientOriginalExtension();
            $new_name = md5(str_random(20) . time()) . '.' . $ex;
            $file->move(public_path('uploads/profiles'), $new_name);
        }
        Profile::create(
            [
                'district_id' => $request->district,
                'name_ar' => $request->name_ar,
                'name_fr' => $request->name_fr,
                'name_en' => $request->name_en,
                'avatar' => $file ? 'uploads/profiles/' . $new_name : null,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
            ]
        );
        session()->flash('message', 'A new profile has been created!');
        return redirect()->route('profiles');
    }

    public function remove($profile_id)
    {
        $profile = Profile::where('id', $profile_id)->first();
        if (is_file($profile->avatar)) {
            unlink(public_path($profile->avatar));
        }
        $profile->delete();
        session()->flash('message', 'A profile has been removed!');
        return redirect()->route('profiles');
    }

}
