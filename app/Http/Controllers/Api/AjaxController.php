<?php

namespace App\Http\Controllers\Api;

use App\Models\Applicant;
use App\Models\Comment;
use App\Models\District;
use App\Models\File;
use App\Models\GovRequest;
use App\Models\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Mail;

class AjaxController extends Controller
{
    public function districts()
    {
        $districts = District::Active()
            ->get();
        return $districts;
    }

    public function get_district(int $district_id)
    {
        $district = District::Active()
            ->where('id', $district_id)
            ->first();
        return $district;
    }

    public function deputes(int $district_id)
    {
        $deputes = User::where('role_id', 2)
            ->where('district_id', $district_id)
            ->get();
        return $deputes;
    }

    public function profiles(int $district_id)
    {
        $profiles = Profile::Active()
            ->where('district_id', $district_id)
            ->get();
        return $profiles;
    }

    public function admins(int $district_id)
    {
        $deputes = User::where('user_type', 'admin')
            ->where('district_id', $district_id)
            ->get();
        return $deputes;
    }

    public function comments(Request $request)
    {
		$exist = \App\Models\Request::where('id', $request->request_id)
		->where('key', $request->cle)
		->first();
		
		if($exist)
		{
        return Comment::where('request_id', $request->request_id)
            ->with('applicant')
            ->with('user')
            ->get();
		}
		else
		{
		return response()->json([ 'error' => 404, 'message' => 'Action non autorisée' ], 404);	
		}
    }

    public function store_comment(Request $request)
    {
		
		$exist = \App\Models\Request::where('id', $request->request_id)
		->where('key', $request->cle)
		->first();
		
		if($exist)
		{
				Comment::create(
					[
						'request_id' => $request->request_id,
						'user_id' => ($request->user != null) ? $request->user['id'] : null,
						'applicant_id' => ($request->applicant != null) ? $request->applicant['id'] : null,
						'body' => $request->body,
						'hidden' => $request->hidden
					]
				);

				$req = \App\Models\Request::where('id', $request->request_id)->first();
				$applicant = Applicant::where('id', $req->applicant->id)->first();
				
				if($req->user !=null)
				$user = User::where('id', $req->user->id)->firstOrFail();
				
				

				if ($request->hidden == false
					&& $req instanceof \App\Models\Request == true
					&& $applicant instanceof Applicant == true
				) {
							
					
					if ($request->applicant != null){
						
					$req->update([
					'conv_status_id' => 1
					]);
						
						if($req->user !=null){
						$this->dispatch(new \App\Jobs\NotifyAdminResp(
							$req->id,
							$user->email
						));
						}
						
					}	
					
					if ($request->user != null){
						
						
						$req->update([
						'conv_status_id' => 2
						]);
						
						
						$this->dispatch(new \App\Jobs\NotifyUser(
							$req->key,
							$applicant->cin,
							$applicant->email,
							$req->title,
							$applicant->name
						));
					}
				}
		}
		else
		{
		return response()->json([ 'error' => 404, 'message' => 'Action non autorisée' ], 404);
		}
		
		
    }

    public function files(Request $request)
    {
		$exist = \App\Models\Request::where('id', $request->request_id)
		->where('key', $request->cle)
		->first();
		
		if($exist)
		{
			$files = File::where('request_id', $request->request_id)
				->get();

			foreach ($files as $key => $file) {
				$files[$key]->src = asset($file->src);
			}

			return $files;
		}
		else
		{
		return response()->json([ 'error' => 404, 'message' => 'Action non autorisée' ], 404);
		}
    }

    public function add_file(Request $request, $request_id)
    {
		$arrA = explode("|", $request_id);
		$exist = \App\Models\Request::where('id', $arrA[0])
		->where('key', $arrA[1])
		->first();
		
		if($exist)
		{
				$this->validate(
					$request,
					[
						'file' => 'required|mimes:doc,docx,jpg,jpeg,pdf,png',
					]
				);

				$file = $request->file('file');
				$ex = $file->getClientOriginalExtension();
				$new_name = md5(str_random(20) . time()) . '.' . $ex;
				$file->move(public_path('uploads'), $new_name);

				$file = File::create(
					[
						'name' => str_replace($ex, '', '.' . $file->getClientOriginalName()),
						'src' => env('APP_URL').'/uploads/' . $new_name,
						'request_id' => $arrA[0]
					]
				);
				

				return $file;
		}
		else
		{
		return response()->json([ 'error' => 404, 'message' => 'Action non autorisée' ], 404);
		}
    }

    public function remove_file(Request $request)
    {
		$exist = \App\Models\Request::where('id', $request->request_id)
		->where('key', $request->cle)
		->first();
		
		if($exist)
		{
			$file = File::where('id', $request->file_id)->first();
			$this->unlink_photo($file->src);
			$file->delete();
			return response()->json(
				[
					'message' => 'done'
				]
			);
		}
		else
		{
		return response()->json([ 'error' => 404, 'message' => 'Action non autorisée' ], 404);
		}
    }

    private function unlink_photo($photo)
    {
        if (is_file($photo)) {
            unlink(public_path($photo));
        }
    }

    public function request(Request $request)
    {
		$exist = \App\Models\Request::where('id', $request->request_id)
		->where('key', $request->cle)
		->first();
		
		if($exist)
		{
			return \App\Models\Request::where('id', $request->request_id)
			->where('key', $request->cle)
			->first();
			
		}
		else
		{
			return response()->json([ 'error' => 404, 'message' => 'Action non autorisée' ], 404);
		}
    }

    public function public_gov_requests(Request $request){
        $govRequest = GovRequest::select('id','title', 'category_id', 'ministry_id', 'created_at')
            ->filter($request->all())
            ->with('user')
            ->with('ministry')
            ->with('category');

        return DataTables::of($govRequest)
            ->addColumn(
                'ministry',
                function ($Request) {
                    if ($Request->ministry) {
                        return $Request->ministry->name_en;
                    }
                    return '--';
                }
            )
            ->addColumn(
                'category',
                function ($Request) {
                    if ($Request->category) {
                        return $Request->category->name;
                    }
                    return '--';
                }
            )
            ->addColumn(
                'document',
                function ($Request) {
                    if ($Request->category) {
                        return $Request->category->name;
                    }
                    return '--';
                }
            )
            ->make(true);
    }
}
