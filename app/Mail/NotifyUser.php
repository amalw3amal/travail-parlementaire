<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Lang;

class NotifyUser extends Mailable
{
    use Queueable, SerializesModels;

    public $key;
    public $cin;
	public $title;
	public $name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($key, $cin, $title, $name)
    {
        $this->key = $key;
        $this->cin = $cin;
		$this->title = $title;
		$this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.notify_user')
            ->subject(Lang::get('layout.notify_user_subject', [], 'ar'))
            ->from(config('mail.from.address'), config('mail.from.name'));
    }
}
