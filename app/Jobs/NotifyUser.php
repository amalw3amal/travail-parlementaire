<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class NotifyUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $key;
    private $cin;
    private $mail;
	private $title;
	private $name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($key, $cin, $mail, $title, $name)
    {
        $this->key = $key;
        $this->cin = $cin;
        $this->mail = $mail;
		$this->title = $title;
		$this->name = $name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->mail)->send(
            new \App\Mail\NotifyUser($this->key, $this->cin, $this->title, $this->name)
        );
    }
}
